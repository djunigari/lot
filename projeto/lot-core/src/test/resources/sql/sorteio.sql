delete from Loteria;
insert into Loteria(id,nome,quantidadeDeDezenas) values (1,'Lotofacil',15);
insert into Loteria(id,nome,quantidadeDeDezenas) values (2,'MegaSena',6);

DELETE FROM TipoGanhador;
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (1,'Ganhadores_11_Números',11);
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (1,'Ganhadores_12_Números',12);
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (1,'Ganhadores_13_Números',13);
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (1,'Ganhadores_14_Números',14);
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (1,'Ganhadores_15_Números',15);
INSERT INTO TipoGanhador(loteriaId,nome,quantidadeDeAcertos) VALUES (2,'Sena',6);


delete from Dezena;
insert into Dezena (loteriaId, numero, ordem) values (2, 1, 1);
insert into Dezena (loteriaId, numero, ordem) values (2, 2, 2);
insert into Dezena (loteriaId, numero, ordem) values (2, 3, 3);
insert into Dezena (loteriaId, numero, ordem) values (2, 4, 4);
insert into Dezena (loteriaId, numero, ordem) values (2, 5, 5);
insert into Dezena (loteriaId, numero, ordem) values (2, 6, 6);
insert into Dezena (loteriaId, numero, ordem) values (2, 7, 1);


insert into Jogo (id, loteria_id) values (1, 2);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 1, 1);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 2, 2);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 3, 3);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 4, 4);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 5, 5);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (1, 2, 6, 6);

insert into Jogo (id, loteria_id) values (2, 2);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 7, 1);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 2, 2);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 3, 3);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 4, 4);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 5, 5);
insert into Jogo_Dezena (Jogo_id, dezenas_loteriaId, dezenas_numero, dezenas_ordem) values (2, 2, 6, 6);


delete from Sorteio;
insert into Sorteio (arrecadacaoTotal, cidade, dataSorteio, estimativaPremio, jogo_id, uf, valorAcumulado, valorAcumuladoEspecial, concurso, loteriaId) values (10.00, 'suzano', '2003-9-29', 1.00, 1, 'sp', '10.00', 2.00, 1, 2);
insert into Sorteio (arrecadacaoTotal, cidade, dataSorteio, estimativaPremio, jogo_id, uf, valorAcumulado, valorAcumuladoEspecial, concurso, loteriaId) values (10.00, 'suzano', '2003-9-29', 1.00, 2, 'sp', '10.00', 2.00, 2, 2);

delete from Combinacao;
insert into Combinacao ( loteriaId, qtdeNumeros,value) values ( 2, 2,'[,,2,,3,,]');
