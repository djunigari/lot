package br.com.lot.core.repositories;

import br.com.lot.core.model.JogoAnalise;
import br.com.lot.core.utils.CombinacaoUtil;
import br.com.lot.utils.BaseApplicationTest;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;

import javax.inject.Inject;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JogoAnaliseRepositoryTest extends BaseApplicationTest {
    @Inject
    private JogoAnaliseRepository repository;
    @Inject
    private JogoRepository jogoRepository;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private CombinacaoRepository combinacaoRepository;

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/createSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/createSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/empty.xml")
    public void createSucessFull(){
        CombinacaoUtil.resetPools();
        JogoAnalise jogoAnalise = new JogoAnalise(jogoRepository.findById(1l));
        jogoAnalise.setCombinacoes(new CombinacaoUtil(jogoAnalise.getJogo().getLoteria(), combinacaoRepository).getCombinacoes(jogoAnalise.getJogo()));
        repository.create(jogoAnalise);
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/notCreateExisting.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/notCreateExistingExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/empty.xml")
    public void notCreateExisting(){
        CombinacaoUtil.resetPools();
        JogoAnalise jogoAnalise = new JogoAnalise(jogoRepository.findById(1l));
        jogoAnalise.setCombinacoes(new CombinacaoUtil(jogoAnalise.getJogo().getLoteria(), combinacaoRepository).getCombinacoes(jogoAnalise.getJogo()));
        repository.create(jogoAnalise);
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/createSecundJogoAnalise.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/createSecundJogoAnaliseExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/JogoAnaliseRepositoryTest/empty.xml")
    public void createSecundJogoAnalise(){
        CombinacaoUtil.resetPools();
        JogoAnalise jogoAnalise = new JogoAnalise(jogoRepository.findById(2l));
        jogoAnalise.setCombinacoes(new CombinacaoUtil(jogoAnalise.getJogo().getLoteria(), combinacaoRepository).getCombinacoes(jogoAnalise.getJogo()));
        repository.create(jogoAnalise);
    }

}
