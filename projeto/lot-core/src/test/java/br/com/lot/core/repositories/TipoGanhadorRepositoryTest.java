package br.com.lot.core.repositories;

import br.com.lot.core.model.TipoGanhador;
import br.com.lot.core.model.pks.TipoGanhadorPK;
import br.com.lot.utils.BaseApplicationTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;

@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/delete_from_all_tables.sql"),
//        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestRun.sql")
})
public class TipoGanhadorRepositoryTest extends BaseApplicationTest{
    @Inject
    private TipoGanhadorRepository repository;

    @BeforeClass
    public void setUp() throws Exception {
//        executeSql("/sql/delete_from_all_tables.sql");
    }

    @Test
    public void createSucessFull(){
        repository.create(TipoGanhador.newTipoGanhador()
                .tipoGanhadorPK(new TipoGanhadorPK("Ganhadores_11_Números", 1))
                .quantidadeDeAcertos(11)
                .build());
        TipoGanhador tipo = repository.findByNameAndLoteria("Ganhadores_11_Números", 1);
        assertEquals(tipo.getQuantidadeDeAcertos().toString(),"11");
        assertEquals(tipo.getTipoGanhadorPK().getNome(),"Ganhadores_11_Números");
        assertEquals(tipo.getTipoGanhadorPK().getLoteriaId().toString(),"1");
    }

    @Test
    public void notCreateExistTipoGanhador(){
        repository.create(TipoGanhador.newTipoGanhador()
                .tipoGanhadorPK(new TipoGanhadorPK("Ganhadores_11_Números", 1))
                .quantidadeDeAcertos(11)
                .build());
        assertEquals(repository.findAll().size(),1);
    }

    @Test
    public void notCreateTipoGanhadorWithDifferentQuantidadeDeAcertos(){
        repository.create(TipoGanhador.newTipoGanhador()
                .tipoGanhadorPK(new TipoGanhadorPK("Ganhadores_11_Números", 1))
                .quantidadeDeAcertos(12)
                .build());
        assertEquals(repository.findAll().size(),1);
    }


    @Test
    public void createTipoGanhadorWithDifferentName(){
        repository.create(TipoGanhador.newTipoGanhador()
                .tipoGanhadorPK(new TipoGanhadorPK("Ganhadores_12_Números", 1))
                .quantidadeDeAcertos(11)
                .build());
        TipoGanhador tipo = repository.findByNameAndLoteria("Ganhadores_12_Números", 1);
        assertEquals(tipo.getQuantidadeDeAcertos().toString(),"11");
        assertEquals(tipo.getTipoGanhadorPK().getNome(), "Ganhadores_12_Números");
        assertEquals(tipo.getTipoGanhadorPK().getLoteriaId().toString(),"1");
    }

    @Test
    public void createTipoGanhadorWithAnotherLoteria(){
        repository.create(TipoGanhador.newTipoGanhador()
                .tipoGanhadorPK(new TipoGanhadorPK("Ganhadores_12_Números", 2))
                .quantidadeDeAcertos(11)
                .build());
        TipoGanhador tipo = repository.findByNameAndLoteria("Ganhadores_12_Números", 2);
        assertEquals(tipo.getQuantidadeDeAcertos().toString(),"11");
        assertEquals(tipo.getTipoGanhadorPK().getNome(), "Ganhadores_12_Números");
        assertEquals(tipo.getTipoGanhadorPK().getLoteriaId().toString(),"2");
    }
}
