package br.com.lot.core.repositories;

import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.pks.DezenaPK;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JogoRepositoryTest extends BaseApplicationTest {
    @Inject
    private SQLFileApplierAction sqlFileApplierAction;
    @Inject
    private JogoRepository repository;
    @Inject
    private LoteriaRepository loteriaRepository;

    @BeforeClass
    public void setUp() throws Exception {
//        executeSql("/sql/delete_from_all_tables.sql");
        executeSql("/sql/loteria.sql");
    }

    @Test
    public void createSucessFull() throws InterruptedException {
        Jogo jogo = repository.create(Jogo.newJogo()
                .loteria(loteriaRepository.findByNome("MegaSena"))
                .dezenas(18, 34, 14, 4, 38, 44)
                .build());

        assertNotNull(jogo.getId());
        assertEquals(jogo.getLoteria().getNome(),"MegaSena");
    }

    @Test
    public void notCreateDuplicateJogo() throws InterruptedException {
        Jogo jogo = repository.create(Jogo.newJogo()
                .loteria(loteriaRepository.findByNome("MegaSena"))
                .dezenas(18, 34, 14, 4, 38, 44)
                .build());

        assertNotNull(jogo.getId());
        assertEquals(jogo.getLoteria().getNome(),"MegaSena");
        assertEquals(repository.findAll().size(), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void notCreateDuplicateDezena() throws InterruptedException {
        Dezena d1 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(34, 2, 2))
                .build();
        Dezena d2 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(34, 2, 2))
                .build();
        Dezena d3 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(14, 3, 2))
                .build();
        Dezena d4 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(4, 4, 2))
                .build();
        Dezena d5 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(38, 5, 2))
                .build();
        Dezena d6 = Dezena.newDezena()
                .dezenaPK(new DezenaPK(44, 6, 2))
                .build();

        repository.create(Jogo.newJogo()
                .loteria(loteriaRepository.findByNome("MegaSena"))
                .addDezena(d1)
                .addDezena(d2)
                .addDezena(d3)
                .addDezena(d4)
                .addDezena(d5)
                .addDezena(d6)
                .build());
    }
}
