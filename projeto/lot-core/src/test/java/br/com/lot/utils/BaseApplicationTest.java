package br.com.lot.utils;

import br.com.lot.StartSpringBootApplication;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import javax.inject.Inject;

import static br.com.lot.utils.ProducerUtils.fetchScript;

@SpringApplicationConfiguration(StartSpringBootApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = "dataSource")
//@WebAppConfiguration
//@IntegrationTest("server.port=9000")
//@ActiveProfiles(value = "test")
//@TestExecutionListeners(inheritListeners = false, listeners = {
//        DependencyInjectionTestExecutionListener.class,
//        DirtiesContextTestExecutionListener.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class BaseApplicationTest{
    @Inject
    private SQLFileApplierAction sqlFileApplierAction;

    protected void executeSql(String path) throws Exception {
        final String script = fetchScript(path);
        sqlFileApplierAction.executeScript(script);
    }
}
