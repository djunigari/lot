package br.com.lot.core.repositories;

import br.com.lot.core.model.CombinacaoAnalise;
import br.com.lot.core.model.Intervalo;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.pks.CombinacaoPK;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.utils.BaseApplicationTest;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;

import javax.inject.Inject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CombinacaoAnaliseRepositoryTest extends BaseApplicationTest{
    @Inject
    private CombinacaoAnaliseRepository repository;
    @Inject
    private CombinacaoRepository combinacaoRepository;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private SorteioRepository sorteioRepository;

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoAnaliseRepositoryTest/createSucessFull.xml")
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoAnaliseRepositoryTest/empty.xml")
    public void createSucessFull(){
        CombinacaoAnalise combinacaoAnalise = new CombinacaoAnalise(combinacaoRepository.findById(new CombinacaoPK(2,"[,,2,,3,,]")));
        combinacaoAnalise.setIntervalos(createIntervalos());
        CombinacaoAnalise result = repository.create(combinacaoAnalise);

        assertNotNull(result.getId());
        assertEquals(result.getSoma().toString(), "5");
        assertEquals(result.getQtdePar().toString(),"1");
        assertEquals(result.getQtdeImpar().toString(),"1");
        assertEquals(result.getMaiorNumero().toString(),"3");
        assertEquals(result.getMenorNumero().toString(),"2");

        assertEquals(new DecimalFormat("#.#").format(result.getMedioNumero()).toString(),"2,5");
    }

    public List<Intervalo> createIntervalos(){
        ArrayList<Intervalo> intervalos = new ArrayList<>();
        Sorteio first = sorteioRepository.findById(new SorteioPK(1, 2));
        Sorteio last = sorteioRepository.findById(new SorteioPK(2, 2));
        intervalos.add(new Intervalo(first, last));
        return intervalos;
    }
}
