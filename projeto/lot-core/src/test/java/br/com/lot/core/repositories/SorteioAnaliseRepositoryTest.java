package br.com.lot.core.repositories;


import br.com.lot.core.model.JogoAnalise;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.SorteioAnalise;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertEquals;

public class SorteioAnaliseRepositoryTest extends BaseApplicationTest {
    @Inject
    private SQLFileApplierAction sqlFileApplierAction;
    @Inject
    private SorteioRepository sorteioRepository;

    @Inject
    private SorteioAnaliseRepository repository;
    @Inject
    private JogoAnaliseRepository jogoAnaliseRepository;

    @BeforeClass
    public void setUp() throws Exception {
//        executeSql("/sql/delete_from_all_tables.sql");
        executeSql("/sql/sorteio.sql");
    }

    @Test
    public void createSucessFull(){
        Sorteio s = sorteioRepository.findById(new SorteioPK(1, 2));
        JogoAnalise jogoAnalise = jogoAnaliseRepository.findByJogo(s.getJogo());
        if(jogoAnalise == null){
            jogoAnalise = new JogoAnalise(s.getJogo());
        }
        SorteioAnalise sorteio = SorteioAnalise.newSorteioAnalise()
                .sorteio(s)
                .jogoAnalise(jogoAnalise)
                .build();

        SorteioAnalise result = repository.create(sorteio);

        assertNotNull(result.getId().toString());
        assertEquals(result.getSorteio().getSorteioPK().getConcurso().toString(), "1");
        assertEquals(result.getSorteio().getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(result.getJogoAnalise().getId().toString(),"1");
    }

    @Test
    public void notCreateSorteioAnaliseExisting(){
        Sorteio s = sorteioRepository.findById(new SorteioPK(1, 2));
        JogoAnalise jogoAnalise = jogoAnaliseRepository.findByJogo(s.getJogo());
        if(jogoAnalise == null){
            jogoAnalise = new JogoAnalise(s.getJogo());
        }

        SorteioAnalise sorteio = SorteioAnalise.newSorteioAnalise()
                .sorteio(s)
                .jogoAnalise(jogoAnalise)
                .build();

        SorteioAnalise result = repository.create(sorteio);

        assertEquals(result.getId().toString(),"1");
        assertEquals(result.getSorteio().getSorteioPK().getConcurso().toString(),"1");
        assertEquals(result.getSorteio().getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(result.getJogoAnalise().getId().toString(),"1");

        assertEquals(repository.findAll().size(),1);
    }

    @Test
    public void createSecundSorteioAnaliseSucessFull(){
        Sorteio s = sorteioRepository.findById(new SorteioPK(2, 2));
        JogoAnalise jogoAnalise = jogoAnaliseRepository.findByJogo(s.getJogo());
        if(jogoAnalise == null){
            jogoAnalise = new JogoAnalise(s.getJogo());
        }
        SorteioAnalise sorteio = SorteioAnalise.newSorteioAnalise()
                .sorteio(s)
                .jogoAnalise(jogoAnalise)
                .build();

        SorteioAnalise result = repository.create(sorteio);

        assertNotNull(result.getId().toString());
        assertEquals(result.getSorteio().getSorteioPK().getConcurso().toString(),"2");
        assertEquals(result.getSorteio().getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(result.getJogoAnalise().getId().toString(),"2");

        assertEquals(repository.findAll().size(),2);
    }
}
