package br.com.lot.rest.converters;

import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.repositories.SorteioRepository;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import br.com.lot.rest.util.FileUtil;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.List;

import static junit.framework.Assert.assertEquals;


public class HtmlToSorteioConverterTest extends BaseApplicationTest {

	@Inject
	private SQLFileApplierAction sqlFileApplierAction;
	@Inject
	private HtmlConverterConfig htmlConverterConfig;
	@Inject
	private HtmlToSorteioConverter converter;
	@Inject
	private SorteioRepository sorteioRepository;
	private Loteria loteria;
	private Jogo jogo;


	@Before
	public void setUp() throws Exception {
		executeSql("/sql/loteria.sql");
		executeSql("/sql/tipo-ganhador.sql");
		htmlConverterConfig.setNomeLoteria("Lotofacil");
	}

	@Test
//	@InSequence(1)
	public void converterHtmlLotofacilToSorteio(){
		String file = new FileUtil().fetchFile("/html/TEST_D_LOTFAC.HTM");
		List<Sorteio> sorteios = converter.convert(file);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Sorteio s1 = sorteios.get(0);
		assertEquals(s1.getSorteioPK().getLoteriaId().toString(),"1");
		assertEquals(s1.getSorteioPK().getConcurso().toString(),"1");
		assertEquals(sdf.format(s1.getDataSorteio()),"29/09/2003");
		assertEquals(s1.getJogo().getDezenas().get(0).getDezenaPK().getNumero().toString(),"18");
		assertEquals(s1.getJogo().getDezenas().get(1).getDezenaPK().getNumero().toString(),"20");
		assertEquals(s1.getJogo().getDezenas().get(2).getDezenaPK().getNumero().toString(),"25");
		assertEquals(s1.getJogo().getDezenas().get(3).getDezenaPK().getNumero().toString(),"23");
		assertEquals(s1.getJogo().getDezenas().get(4).getDezenaPK().getNumero().toString(),"10");
		assertEquals(s1.getJogo().getDezenas().get(5).getDezenaPK().getNumero().toString(),"11");
		assertEquals(s1.getJogo().getDezenas().get(6).getDezenaPK().getNumero().toString(),"24");
		assertEquals(s1.getJogo().getDezenas().get(7).getDezenaPK().getNumero().toString(),"14");
		assertEquals(s1.getJogo().getDezenas().get(8).getDezenaPK().getNumero().toString(),"6");
		assertEquals(s1.getJogo().getDezenas().get(9).getDezenaPK().getNumero().toString(),"2");
		assertEquals(s1.getJogo().getDezenas().get(10).getDezenaPK().getNumero().toString(),"13");
		assertEquals(s1.getJogo().getDezenas().get(11).getDezenaPK().getNumero().toString(),"9");
		assertEquals(s1.getJogo().getDezenas().get(12).getDezenaPK().getNumero().toString(),"5");
		assertEquals(s1.getJogo().getDezenas().get(13).getDezenaPK().getNumero().toString(),"16");
		assertEquals(s1.getJogo().getDezenas().get(14).getDezenaPK().getNumero().toString(),"3");
		assertEquals(s1.getArrecadacaoTotal().toString(),"0.00");
		assertEquals(s1.getGanhadores().get(4).getQuantidadeDeGanhadores().toString(),"5");
		assertEquals(s1.getGanhadores().get(3).getQuantidadeDeGanhadores().toString(),"154");
		assertEquals(s1.getGanhadores().get(2).getQuantidadeDeGanhadores().toString(),"4645");
		assertEquals(s1.getGanhadores().get(1).getQuantidadeDeGanhadores().toString(),"48807");
		assertEquals(s1.getGanhadores().get(0).getQuantidadeDeGanhadores().toString(),"257593");

		assertEquals(s1.getGanhadores().get(4).getPremioParaCada().toString(),"49765.82");
		assertEquals(s1.getGanhadores().get(3).getPremioParaCada().toString(),"689.84");
		assertEquals(s1.getGanhadores().get(2).getPremioParaCada().toString(),"10.00");
		assertEquals(s1.getGanhadores().get(1).getPremioParaCada().toString(),"4.00");
		assertEquals(s1.getGanhadores().get(0).getPremioParaCada().toString(),"2.00");

		assertEquals(s1.getValorAcumulado().toString(),"0.00");
		assertEquals(s1.getEstimativaPremio().toString(),"0.00");
		assertEquals(s1.getValorAcumuladoEspecial().toString(),"0.00");


		Sorteio s2 = sorteios.get(1);
		assertEquals(s2.getSorteioPK().getLoteriaId().toString(),"1");
		assertEquals(s2.getSorteioPK().getConcurso().toString(),"2");
		assertEquals(sdf.format(s2.getDataSorteio()),"06/10/2003");
		assertEquals(s2.getJogo().getDezenas().get(0).getDezenaPK().getNumero().toString(),"23");
		assertEquals(s2.getJogo().getDezenas().get(1).getDezenaPK().getNumero().toString(),"15");
		assertEquals(s2.getJogo().getDezenas().get(2).getDezenaPK().getNumero().toString(),"5");
		assertEquals(s2.getJogo().getDezenas().get(3).getDezenaPK().getNumero().toString(),"4");
		assertEquals(s2.getJogo().getDezenas().get(4).getDezenaPK().getNumero().toString(),"12");
		assertEquals(s2.getJogo().getDezenas().get(5).getDezenaPK().getNumero().toString(),"16");
		assertEquals(s2.getJogo().getDezenas().get(6).getDezenaPK().getNumero().toString(),"20");
		assertEquals(s2.getJogo().getDezenas().get(7).getDezenaPK().getNumero().toString(),"6");
		assertEquals(s2.getJogo().getDezenas().get(8).getDezenaPK().getNumero().toString(),"11");
		assertEquals(s2.getJogo().getDezenas().get(9).getDezenaPK().getNumero().toString(),"19");
		assertEquals(s2.getJogo().getDezenas().get(10).getDezenaPK().getNumero().toString(),"24");
		assertEquals(s2.getJogo().getDezenas().get(11).getDezenaPK().getNumero().toString(),"1");
		assertEquals(s2.getJogo().getDezenas().get(12).getDezenaPK().getNumero().toString(),"9");
		assertEquals(s2.getJogo().getDezenas().get(13).getDezenaPK().getNumero().toString(),"13");
		assertEquals(s2.getJogo().getDezenas().get(14).getDezenaPK().getNumero().toString(),"7");
		assertEquals(s2.getArrecadacaoTotal().toString(),"0.00");
		assertEquals(s2.getGanhadores().get(4).getQuantidadeDeGanhadores().toString(),"1");
		assertEquals(s2.getGanhadores().get(3).getQuantidadeDeGanhadores().toString(),"184");
		assertEquals(s2.getGanhadores().get(2).getQuantidadeDeGanhadores().toString(),"6232");
		assertEquals(s2.getGanhadores().get(1).getQuantidadeDeGanhadores().toString(),"81252");
		assertEquals(s2.getGanhadores().get(0).getQuantidadeDeGanhadores().toString(),"478188");

		assertEquals(s2.getGanhadores().get(4).getPremioParaCada().toString(),"596323.70");
		assertEquals(s2.getGanhadores().get(3).getPremioParaCada().toString(),"1388.95");
		assertEquals(s2.getGanhadores().get(2).getPremioParaCada().toString(),"10.00");
		assertEquals(s2.getGanhadores().get(1).getPremioParaCada().toString(),"4.00");
		assertEquals(s2.getGanhadores().get(0).getPremioParaCada().toString(),"2.00");

		assertEquals(s2.getValorAcumulado().toString(),"0.00");
		assertEquals(s2.getEstimativaPremio().toString(),"0.00");
		assertEquals(s2.getValorAcumuladoEspecial().toString(),"0.00");

		jogo = s1.getJogo();
	}
}
