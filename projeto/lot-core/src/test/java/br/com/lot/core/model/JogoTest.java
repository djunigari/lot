package br.com.lot.core.model;

import br.com.lot.core.model.pks.DezenaPK;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class JogoTest {
    private Jogo jogo;

    @Before
    public void setUp(){
        this.jogo = Jogo.newJogo()
                .dezenas(dezenas())
                .build();
    }

    @Test
    public void hasDezena(){
        Dezena d = Dezena.newDezena()
                .dezenaPK(new DezenaPK(1, 1, 1))
                .build();

        assertTrue(this.jogo.hasDezena(d));
    }

    @Test
    public void notHasDezena(){
        Dezena d = Dezena.newDezena()
                .dezenaPK(new DezenaPK(60, 1, 1))
                .build();

        assertFalse(this.jogo.hasDezena(d));
    }

    @Test
    public void hasDezenas(){
        assertTrue(this.jogo.hasDezenas(dezenas()));
    }

    @Test
    public void notHasOneOfListDezenas(){
        List<Dezena> dezenas = dezenas();
        dezenas.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(60, 1, 1))
                .build());

        assertFalse(this.jogo.hasDezenas(dezenas));
    }

    private List<Dezena> dezenas(){
        List<Dezena> list = new ArrayList<>();
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(1,1,1))
                .build());
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(2,2,1))
                .build());
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(3,3,1))
                .build());
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(4,4,1))
                .build());
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(5,5,1))
                .build());
        list.add(Dezena.newDezena()
                .dezenaPK(new DezenaPK(6,6,1))
                .build());
        return list;
    }
}
