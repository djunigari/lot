package br.com.lot.core.repositories;

import br.com.lot.core.model.Combinacao;
import br.com.lot.utils.BaseApplicationTest;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;

public class CombinacaoRepositoryTest extends BaseApplicationTest {
    @Inject
    private CombinacaoRepository repository;
    @Inject
    private LoteriaRepository loteriaRepository;

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/createSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/createSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void createSucessFull(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();
        repository.create(combinacao);
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreateDuplicateCombinacao.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreateDuplicateCombinacao.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void notCreateDuplicateCombinacao(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(2)
                .addNumeros(2)
                .addNumeros(1)
                .build();
        repository.create(combinacao);
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreateDuplicateCombinacao.xml")
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void findByNumeros(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(1);
        List<Combinacao> combinacoes = repository.findByLoteriaIdAndNumeros(loteriaRepository.findByNome("Lotofacil").getId(),numeros);
        assertEquals(combinacoes.get(0).getCombinacaoPK().getLoteriaId(),loteriaRepository.findByNome("Lotofacil").getId());
        assertEquals(combinacoes.get(0).getNumeros().get(0).toString(),"1");
        assertEquals(combinacoes.get(0).getNumeros().get(1).toString(),"2");
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/createWithOneMoreNumberSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/createWithOneMoreNumberSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void createWithOneMoreNumberSucessFull(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(3)
                .addNumeros(1)
                .addNumeros(3)
                .addNumeros(2)
                .build();
         repository.create(combinacao);
    }

    @Test(expected = IllegalArgumentException.class)
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void notCreateWithMoreNumberDifferentQtdeNumeros(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .addNumeros(3)
                .build();
        repository.create(combinacao);
    }

    @Test(expected = IllegalArgumentException.class)
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void notCreateWithNumberDifferentQtdeNumeros(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .addNumeros(3)
                .build();
        repository.create(combinacao);
    }

    @Test(expected= IllegalArgumentException.class)
    @DatabaseSetup(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/notCreate.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/CombinacaoRepositoryTest/empty.xml")
    public void notCreateWithDuplicateNumbers(){
        Combinacao combinacao = Combinacao.newCombinacao()
                .loteria(loteriaRepository.findByNome("Lotofacil"))
                .qtdeNumeros(2)
                .addNumeros(2)
                .addNumeros(2)
                .build();
        repository.create(combinacao);
    }
}
