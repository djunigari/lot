package br.com.lot.core.model;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class CombinacaoTest {
    private Combinacao combinacao;
    private Loteria loteria;

    @Before
    public void setUp(){
        this.loteria = Loteria.newLoteria().id(1).quantidadeDeDezenas(6).nome("MegaSena").build();
        this.combinacao = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();
    }

    @Test
    public void hasNumero(){
        assertTrue(this.combinacao.hasNumero(1));
    }

    @Test
    public void notHasNumero(){
        assertFalse(this.combinacao.hasNumero(40));
    }

    @Test
    public void hasNumeros(){
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        assertTrue(this.combinacao.hasNumeros(numeros));
    }

    @Test
    public void notHasNumeros(){
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(40);
        assertFalse(this.combinacao.hasNumeros(numeros));
    }

    @Test
    public void equals(){
        Loteria loteria = Loteria.newLoteria().id(1).nome("MegaSena").quantidadeDeDezenas(6).build();

        Combinacao c1 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();

        Combinacao c2 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();
        assertTrue(c1.equals(c2));
    }

    @Test
    public void notEqualsWithDifferentLoteria(){
        Loteria loteria1 = Loteria.newLoteria().id(1).build();
        Loteria loteria2 = Loteria.newLoteria().id(2).build();
        Combinacao c1 = Combinacao.newCombinacao()
                .loteria(loteria1)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();

        Combinacao c2 = Combinacao.newCombinacao()
                .loteria(loteria2)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();
        assertFalse(c1.equals(c2));
    }

    @Test
    public void notEqualsWithDifferentQtdeNumeros(){
        Loteria loteria = Loteria.newLoteria().id(1).build();
        Combinacao c1 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();

        Combinacao c2 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(3)
                .addNumeros(1)
                .addNumeros(2)
                .build();
        assertFalse(c1.equals(c2));
    }

    @Test
    public void notEqualsWithDifferentNumeros(){
        Loteria loteria = Loteria.newLoteria().id(1).build();
        Combinacao c1 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(2)
                .build();

        Combinacao c2 = Combinacao.newCombinacao()
                .loteria(loteria)
                .qtdeNumeros(2)
                .addNumeros(1)
                .addNumeros(24)
                .build();
        assertFalse(c1.equals(c2));
    }
}
