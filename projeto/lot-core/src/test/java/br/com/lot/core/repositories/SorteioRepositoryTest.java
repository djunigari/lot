package br.com.lot.core.repositories;

import br.com.lot.core.model.Ganhador;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.pks.CombinacaoPK;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.core.services.SorteioAnaliseService;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;

public class SorteioRepositoryTest extends BaseApplicationTest {
    @Inject
    private SQLFileApplierAction sqlFileApplierAction;
    @Inject
    private SorteioRepository repository;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private TipoGanhadorRepository tipoGanhadorRepository;
    @Inject
    private SorteioAnaliseService service;


    @BeforeClass
    public void setUp() throws Exception {
//        executeSql("/sql/delete_from_all_tables.sql");
        executeSql("/sql/loteria.sql");
        executeSql("/sql/tipo-ganhador.sql");
    }

    @Test
    public void createSucessFull(){
        Sorteio sorteio = repository.create(createSorteio());
        Sorteio s1 = repository.findById(sorteio.getSorteioPK());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        assertEquals(s1.getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(s1.getSorteioPK().getConcurso().toString(),"1");
        assertEquals(sdf.format(s1.getDataSorteio()),"29/09/2003");
        assertEquals(s1.getJogo().getDezenas().get(0).getDezenaPK().getNumero().toString(),"18");
        assertEquals(s1.getJogo().getDezenas().get(1).getDezenaPK().getNumero().toString(),"20");
        assertEquals(s1.getJogo().getDezenas().get(2).getDezenaPK().getNumero().toString(),"25");
        assertEquals(s1.getJogo().getDezenas().get(3).getDezenaPK().getNumero().toString(),"23");
        assertEquals(s1.getJogo().getDezenas().get(4).getDezenaPK().getNumero().toString(),"10");
        assertEquals(s1.getJogo().getDezenas().get(5).getDezenaPK().getNumero().toString(),"11");
        assertEquals(s1.getArrecadacaoTotal().toString(),"10.00");
        assertEquals(s1.getGanhadores().get(0).getQuantidadeDeGanhadores().toString(),"3");
        assertEquals(s1.getGanhadores().get(0).getPremioParaCada().toString(),"2.00");

        assertEquals(s1.getValorAcumulado().toString(),"10.00");
        assertEquals(s1.getEstimativaPremio().toString(),"1.00");
        assertEquals(s1.getValorAcumuladoEspecial().toString(),"2.00");
    }

    @Test
    public void createSecundSorteioSucessFull(){
        Sorteio sorteio = createSorteio();
        sorteio.setSorteioPK(new SorteioPK(2,loteriaRepository.findByNome("MegaSena").getId()));
        sorteio = repository.create(sorteio);

        Sorteio s1 = repository.findById(sorteio.getSorteioPK());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        assertEquals(s1.getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(s1.getSorteioPK().getConcurso().toString(),"2");
        assertEquals(sdf.format(s1.getDataSorteio()),"29/09/2003");
        assertEquals(s1.getJogo().getDezenas().get(0).getDezenaPK().getNumero().toString(),"18");
        assertEquals(s1.getJogo().getDezenas().get(1).getDezenaPK().getNumero().toString(),"20");
        assertEquals(s1.getJogo().getDezenas().get(2).getDezenaPK().getNumero().toString(),"25");
        assertEquals(s1.getJogo().getDezenas().get(3).getDezenaPK().getNumero().toString(),"23");
        assertEquals(s1.getJogo().getDezenas().get(4).getDezenaPK().getNumero().toString(),"10");
        assertEquals(s1.getJogo().getDezenas().get(5).getDezenaPK().getNumero().toString(),"11");
        assertEquals(s1.getArrecadacaoTotal().toString(),"10.00");
        assertEquals(s1.getGanhadores().get(0).getQuantidadeDeGanhadores().toString(),"3");
        assertEquals(s1.getGanhadores().get(0).getPremioParaCada().toString(),"2.00");

        assertEquals(s1.getValorAcumulado().toString(),"10.00");
        assertEquals(s1.getEstimativaPremio().toString(),"1.00");
        assertEquals(s1.getValorAcumuladoEspecial().toString(),"2.00");

        Sorteio s2 = repository.findById(new SorteioPK(1,loteriaRepository.findByNome("MegaSena").getId()));
        assertEquals(s1.getJogo().getId(), s2.getJogo().getId());

        assertEquals(repository.findAll().size(),2);
    }

    @Test
    public void findLastSorteioByCombinacaoId(){
        Sorteio sorteio = createSorteio();
        sorteio.setSorteioPK(new SorteioPK(3,loteriaRepository.findByNome("MegaSena").getId()));
        sorteio = repository.create(sorteio);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        service.createAllAnalises();
        Sorteio last = repository.findLastSorteioByCombinacaoId(new CombinacaoPK(2, "[,,18,,20,,]"));

        assertEquals(last.getSorteioPK().getLoteriaId().toString(),sorteio.getSorteioPK().getLoteriaId().toString());
        assertEquals(last.getSorteioPK().getConcurso().toString(),sorteio.getSorteioPK().getConcurso().toString());
        assertEquals(last.getSorteioPK().getConcurso().toString(),sorteio.getSorteioPK().getConcurso().toString());
    }

    public Sorteio createSorteio(){
        SorteioPK sorteioPK = new SorteioPK(1,loteriaRepository.findByNome("MegaSena").getId());
        return Sorteio.newSorteio()
                .sorteioPK(sorteioPK)
                .dataSorteio(new Date("2003/9/29"))
                .jogo(createJogo())
                .arrecadacaoTotal(new BigDecimal("10.00"))
                .cidade("")
                .uf("sp")
                .ganhadores(createGanhadores())
                .valorAcumulado(new BigDecimal("10.00"))
                .estimativaPremio(new BigDecimal("1.00"))
                .valorAcumuladoEspecial(new BigDecimal("2.00"))
                .build();
    }

    public Jogo createJogo(){
        return Jogo.newJogo()
                .loteria(loteriaRepository.findByNome("MegaSena"))
                .dezenas(18,20,25,23,10,11)
                .build();
    }

    public List<Ganhador> createGanhadores(){
        ArrayList<Ganhador> ganhadores = new ArrayList<>();
        Ganhador g1 = Ganhador.newGanhador()
                .quantidadeDeGanhadores(3)
                .tipoDeGanhador(tipoGanhadorRepository.findByNameAndLoteria("Ganhadores_Sena",loteriaRepository.findByNome("MegaSena").getId()))
                .premioParaCada(new BigDecimal("2.00"))
                .build();

        ganhadores.add(g1);
        return ganhadores;
    }
 }
