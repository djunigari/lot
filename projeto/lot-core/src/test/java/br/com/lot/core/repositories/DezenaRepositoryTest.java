package br.com.lot.core.repositories;

import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.pks.DezenaPK;
import br.com.lot.utils.BaseApplicationTest;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;

import javax.inject.Inject;

import static junit.framework.TestCase.assertEquals;

public class DezenaRepositoryTest extends BaseApplicationTest{
    @Inject
    private DezenaRepository repository;


    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/empty.xml")
    public void createSucessFull() throws InterruptedException {
        repository.create(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(18, 1, 1))
                        .build()
        );
    }


   @Test
   @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSucessFullExpected.xml")
   @DatabaseTearDown(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/empty.xml")
    public void findDezenaSucessFull() throws InterruptedException {
        Dezena dezena = repository.find(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(18, 1, 1))
                        .build()
        );

        assertEquals(dezena.getDezenaPK().getLoteriaId().toString(),"1");
        assertEquals(dezena.getDezenaPK().getNumero().toString(),"18");
        assertEquals(dezena.getDezenaPK().getOrdem().toString(),"1");
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSucessFull.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void notCreateDuplicateDezenaSucessFull() throws InterruptedException {
        repository.create(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(18, 1, 1))
                        .build());
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteOrdemSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteOrdemSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/empty.xml")
    public void createSecundDezenaWithDifferenteOrdemSucessFull() throws InterruptedException {
        repository.create(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(18, 2, 1))
                        .build()
        );
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteLoteriaSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteLoteriaSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/empty.xml")
    public void createSecundDezenaWithDifferenteLoteriaSucessFull() throws InterruptedException {
        repository.create(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(18, 2, 2))
                        .build()
        );
    }

    @Test
    @DatabaseSetup(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteNumeroSucessFull.xml")
    @ExpectedDatabase(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/createSecundDezenaWithDifferenteNumeroSucessFullExpected.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    @DatabaseTearDown(value = "classpath:datasets/core/repositories/DezenaRepositoryTest/empty.xml")
    public void createSecundDezenaWithDifferenteNumeroSucessFull() throws InterruptedException {
        repository.create(Dezena.newDezena()
                        .dezenaPK(new DezenaPK(19, 1, 1))
                        .build()
        );
    }
}
