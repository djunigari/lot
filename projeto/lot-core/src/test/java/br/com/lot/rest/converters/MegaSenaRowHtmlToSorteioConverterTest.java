package br.com.lot.rest.converters;

import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.repositories.SorteioRepository;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import br.com.lot.rest.util.FileUtil;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.List;

import static junit.framework.Assert.assertEquals;

public class MegaSenaRowHtmlToSorteioConverterTest extends BaseApplicationTest {
    @Inject
    private SQLFileApplierAction sqlFileApplierAction;
    @Inject
    private HtmlConverterConfig htmlConverterConfig;
    @Inject
    private HtmlToSorteioConverter converter;
    @Inject
    private SorteioRepository sorteioRepository;
    private Loteria loteria;
    private Jogo jogo;


    @Before
    public void setUp() throws Exception {
        executeSql("/sql/loteria.sql");
        executeSql("/sql/tipo-ganhador.sql");
        htmlConverterConfig.setNomeLoteria("MegaSena");
    }

    @Test
//    @InSequence(1)
    public void converterHtmlLotofacilToSorteio(){
        String file = new FileUtil().fetchFile("/html/TEST_D_MEGA.HTM");
        List<Sorteio> sorteios = converter.convert(file);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Sorteio s1 = sorteios.get(0);
        assertEquals(s1.getSorteioPK().getLoteriaId().toString(),"2");
        assertEquals(s1.getSorteioPK().getConcurso().toString(),"1");
        assertEquals(sdf.format(s1.getDataSorteio()),"11/03/1996");
        assertEquals(s1.getJogo().getDezenas().get(0).getDezenaPK().getNumero().toString(),"41");
        assertEquals(s1.getJogo().getDezenas().get(1).getDezenaPK().getNumero().toString(),"5");
        assertEquals(s1.getJogo().getDezenas().get(2).getDezenaPK().getNumero().toString(),"4");
        assertEquals(s1.getJogo().getDezenas().get(3).getDezenaPK().getNumero().toString(),"52");
        assertEquals(s1.getJogo().getDezenas().get(4).getDezenaPK().getNumero().toString(),"30");
        assertEquals(s1.getJogo().getDezenas().get(5).getDezenaPK().getNumero().toString(),"33");
        assertEquals(s1.getArrecadacaoTotal().toString(),"0.00");
        assertEquals(s1.getGanhadores().get(2).getQuantidadeDeGanhadores().toString(),"0");
        assertEquals(s1.getGanhadores().get(1).getQuantidadeDeGanhadores().toString(),"17");
        assertEquals(s1.getGanhadores().get(0).getQuantidadeDeGanhadores().toString(),"2016");

        assertEquals(s1.getGanhadores().get(2).getPremioParaCada().toString(),"0.00");
        assertEquals(s1.getGanhadores().get(1).getPremioParaCada().toString(),"39158.92");
        assertEquals(s1.getGanhadores().get(0).getPremioParaCada().toString(),"330.21");

        assertEquals(s1.getValorAcumulado().toString(),"1714650.23");
        assertEquals(s1.getEstimativaPremio().toString(),"0.00");
        assertEquals(s1.getValorAcumuladoEspecial().toString(),"0.00");
    }
}
