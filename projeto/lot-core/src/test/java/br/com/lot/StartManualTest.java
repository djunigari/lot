package br.com.lot;

import br.com.lot.core.model.Sorteio;
import br.com.lot.core.repositories.CombinacaoAnaliseRepository;
import br.com.lot.core.repositories.CombinacaoRepository;
import br.com.lot.core.repositories.SorteioRepository;
import br.com.lot.core.services.CombinacaoAnaliseService;
import br.com.lot.core.services.SorteioAnaliseService;
import br.com.lot.rest.converters.HtmlToSorteioConverter;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import br.com.lot.rest.util.FileUtil;
import br.com.lot.utils.BaseApplicationTest;
import br.com.lot.utils.SQLFileApplierAction;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;

import javax.inject.Inject;
import java.util.List;

@SpringApplicationConfiguration(StartSpringBootApplication.class)
public class StartManualTest extends BaseApplicationTest{

    @Inject
    private SQLFileApplierAction sqlFileApplierAction;
    @Inject
    private HtmlConverterConfig htmlConverterConfig;
    @Inject
    private HtmlToSorteioConverter converter;
    @Inject
    private SorteioRepository sorteioRepository;
    @Inject
    private CombinacaoAnaliseRepository combinacaoAnaliseRepository;
    @Inject
    private CombinacaoRepository combinacaoRepository;
    @Inject
    private SorteioAnaliseService service;
    @Inject
    private CombinacaoAnaliseService combinacaoAnaliseService;


    @BeforeClass
    public void setUp() throws Exception {
        executeSql("/sql/loteria.sql");
        executeSql("/sql/tipo-ganhador.sql");
        htmlConverterConfig.setNomeLoteria("MegaSena");
    }


    @Test
    public void dontStop(){
        String file = new FileUtil().fetchFile("/html/D_MEGA.HTM");
        List<Sorteio> sorteios = converter.convert(file);
        sorteioRepository.create(sorteios);
        service.createAllAnalises();
//        combinacaoAnaliseService.create();
//        List<CombinacaoAnalise> all = combinacaoAnaliseRepository.findAll();
//        System.out.println(all.size());
//        System.out.println("");

//        System.out.print("Started Arquillian");
//        Thread.sleep(Long.MAX_VALUE);
    }
}
