package br.com.lot.core.repositories;

import br.com.lot.core.model.Ganhador;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Transactional
public class GanhadorRepository extends  JPARepository<Ganhador,Integer>{
    @Inject
    private EntityManager em;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    public List<Ganhador> create(List<Ganhador> ganhadores){
        for(Ganhador g : ganhadores){
            create(g);
        }
        return ganhadores;
    }

    @Override
    protected Ganhador find(Ganhador ganhador) {
        if(ganhador.getId() == null){
            return null;
        }
        return findById(ganhador.getId());
    }
}
