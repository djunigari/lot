package br.com.lot.rest.base;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Resource<T extends Item> implements Serializable {
    @JsonProperty("uri")
    private String uri;
    @JsonProperty("item")
    private T item;
    @JsonProperty("errors")
    private Errors errors;
    @JsonProperty("items")
    private Collection<T> items;
    @JsonProperty("links")
    protected Map<String, Link> links;

    @JsonIgnore
    private int page;
    @JsonIgnore
    private int pageSize;
    @JsonProperty("total-size")
    private Long totalSize;

    public Resource() {
    }

    public Resource(T item) {
        this.item = item;
        this.uri = item.getUri();
    }

    public Resource(String uri, Collection<T> items) {
        this.uri = uri;
        this.items = items;
    }

    public Resource(String uri, String resource, Collection<T> items, long totalSize, int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
        this.uri = uri;
        this.items = items;
        this.totalSize = totalSize;

        if (!items.isEmpty()) {
            buildLinks(resource);
        }
    }

    public Resource(String uri, Collection<T> items, Map<String, Link> links) {
        this.uri = uri;
        this.items = items;
        this.links = links;
    }

    public Resource(String uri, Errors errors) {
        this.uri = uri;
        this.errors = errors;
    }

    public String getUri() {
        return uri;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public Map<String, Link>  getLinks() {
        return links;
    }

    public T getItem() {
        return this.item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public Collection<T> getItems() {
        return items;
    }

    public void setItems(Collection<T> items) {
        this.items = items;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public Resource<T> addLink(String rel, String href) {
        if (this.links == null) {
            this.links = new HashMap<String, Link>();
        }

        this.links.put(rel, new Link(href));
        return this;
    }

    private void buildLinks(String resource) {
        if (page != 1) {
            StringBuilder linkBuilder = new StringBuilder();

            linkBuilder
                    .append(resource)
                    .append("?page=")
                    .append(page - 1)
                    .append("&page-size=")
                    .append(pageSize);

            addLink("previous", linkBuilder.toString());
        }

        if (totalSize > (page * pageSize)) {
            StringBuilder linkBuilder = new StringBuilder();

            linkBuilder
                    .append(resource)
                    .append("?page=")
                    .append(page + 1)
                    .append("&page-size=")
                    .append(pageSize);

            addLink("next", linkBuilder.toString());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Resource)) return false;

        Resource resource = (Resource) o;

        if (totalSize != resource.totalSize) return false;
        if (errors != null ? !errors.equals(resource.errors) : resource.errors != null) return false;
        if (item != null ? !item.equals(resource.item) : resource.item != null) return false;
        if (items != null ? !items.equals(resource.items) : resource.items != null) return false;
        if (links != null ? !links.equals(resource.links) : resource.links != null) return false;
        if (uri != null ? !uri.equals(resource.uri) : resource.uri != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uri != null ? uri.hashCode() : 0;
        result = 31 * result + (item != null ? item.hashCode() : 0);
        result = 31 * result + (errors != null ? errors.hashCode() : 0);
        result = 31 * result + (items != null ? items.hashCode() : 0);
        result = 31 * result + (int) (totalSize ^ (totalSize >>> 32));
        result = 31 * result + (links != null ? links.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Resource{" +
                "uri='" + uri + '\'' +
                ", item=" + item +
                ", errors=" + errors +
                ", items=" + items +
                ", totalSize=" + totalSize +
                ", links=" + links +
                '}';
    }
}
