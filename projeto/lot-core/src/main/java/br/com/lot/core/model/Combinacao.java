package br.com.lot.core.model;

import br.com.lot.core.model.pks.CombinacaoPK;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = Combinacao.FIND_BY_LOTERIA_ID_AND_HASH,
                query = "From Combinacao c where c.combinacaoPK.loteriaId = :loteriaId and  c.combinacaoPK.value = :value"),
        @NamedQuery(name = Combinacao.FIND_BY_LOTERIA_ID_AND_NUMEROS,
                query = "From Combinacao c where c.combinacaoPK.loteriaId = :loteriaId and  c.combinacaoPK.value like :value"),
})
@Table(name = "COMBINACAO")
public class Combinacao implements Comparable<Combinacao>{
    public static final String FIND_BY_LOTERIA_ID_AND_HASH = "Combinacao.findByLoteriaIdAndHash";
    public static final String FIND_BY_LOTERIA_ID_AND_NUMEROS = "findByLoteriaIdAndNumeros";

    @EmbeddedId
    private CombinacaoPK combinacaoPK;
    @Column(name="QTDE_NUMEROS")
    private Integer qtdeNumeros;
//    @ElementCollection
//    @CollectionTable(name ="numeros")
//    @LazyCollection(LazyCollectionOption.FALSE)
    @Transient
    private List<Integer> numeros;

    public Combinacao(){}

    private Combinacao(Builder builder) {
        this.combinacaoPK = builder.pk;
        this.qtdeNumeros = builder.qtdeNumeros;
        this.numeros = builder.numeros;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Combinacao)){
            return false;
        }
        Combinacao combinacao = (Combinacao) obj;
        if(!combinacao.getCombinacaoPK().equals(combinacaoPK)){
            return false;
        }
        if(!combinacao.getQtdeNumeros().equals(getQtdeNumeros())){
            return false;
        }
        return true;
    }

    public static Builder newCombinacao() {
        return new Builder();
    }

    public Integer getQtdeNumeros() {
        return qtdeNumeros;
    }

    public List<Integer> getNumeros() {

        if(numeros == null) {
            numeros = new ArrayList<>();
            String[] split = combinacaoPK.getValue().replace("[,,", "").replace(",,]", "").split(",,");
            for(String n : split){
                numeros.add(Integer.parseInt(n));
            }
        }
        return numeros;
    }

//    public void setNumeros(List<Integer> numeros) {
//        if(numeros.size() > qtdeNumeros){
//            throw new IllegalArgumentException();
//        }
//        this.numeros = numeros;
//    }
//
//    public void addNumero(Integer numero){
//        if(numeros.size() == qtdeNumeros){
//            throw new IllegalArgumentException();
//        }
//        numeros.add(numero);
//    }


    public boolean hasNumeros(List<Integer> numeros) {
        for(Integer numero : numeros){
            if(!hasNumero(numero)){
                return false;
            }
        }
        return true;
    }

    public boolean hasNumero(Integer numero){
        for(Integer n : getNumeros()){
            if(n.equals(numero)){
                return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(Combinacao c) {
        if(qtdeNumeros - c.getQtdeNumeros() != 0){
            return qtdeNumeros - c.getQtdeNumeros();
        }
        for(int i = 0; i < qtdeNumeros ; i++){
            if(getNumeros().get(i) - c.getNumeros().get(i) != 0){
                return getNumeros().get(i) - c.getNumeros().get(i);
            }
        }
        return 0;
    }

    public CombinacaoPK getCombinacaoPK() {
        return combinacaoPK;
    }

public static final class Builder {
        private CombinacaoPK pk;
        private Loteria loteria;
        private Integer qtdeNumeros;
        private List<Integer> numeros;

        private Builder() {
            numeros = new ArrayList<>();
        }

        public Combinacao build() {
            Collections.sort(numeros);
            StringBuilder value = new StringBuilder("[,");
            for (Integer i : numeros) {
                value.append(",").append(i).append(",");
            }
            value.append(",]");
            this.pk = new CombinacaoPK(this.loteria.getId(), value.toString());
            return new Combinacao(this);
        }

        public Builder id(CombinacaoPK pk) {
            this.pk = pk;
            return this;
        }

        public Builder loteria(Loteria loteria) {
            this.loteria = loteria;
            return this;
        }

        public Builder qtdeNumeros(Integer qtdeNumeros) {
            this.qtdeNumeros = qtdeNumeros;
            return this;
        }

        public Builder numeros(List<Integer> numeros) {
            this.numeros = numeros;
            return this;
        }

        public Builder addNumeros(Integer numero) {
            if(this.numeros.size() == qtdeNumeros){
                throw new IllegalArgumentException();
            }
            for(Integer n : this.numeros){
                if(n.equals(numero)) {
                    throw new IllegalArgumentException();
                }
            }
            this.numeros.add(numero);
            return this;
        }
    }
}
