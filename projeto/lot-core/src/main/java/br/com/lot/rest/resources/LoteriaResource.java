package br.com.lot.rest.resources;

import br.com.lot.rest.base.Item;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonRootName("loteria")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoteriaResource extends Item<Long> {
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("quantidade-de-dezenas")
    private Integer quantidadeDeDezenas;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadeDeDezenas() {
        return quantidadeDeDezenas;
    }

    public void setQuantidadeDeDezenas(Integer quantidadeDeDezenas) {
        this.quantidadeDeDezenas = quantidadeDeDezenas;
    }
}
