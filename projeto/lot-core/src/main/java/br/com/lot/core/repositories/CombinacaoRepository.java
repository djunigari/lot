package br.com.lot.core.repositories;

import br.com.lot.core.model.Combinacao;
import br.com.lot.core.model.pks.CombinacaoPK;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Repository
@Transactional
public class CombinacaoRepository extends JPARepository<Combinacao,CombinacaoPK>{
    @Inject
    private EntityManager em;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    @Override
    public Combinacao create(Combinacao combinacao) {
        List<Combinacao> combinacoes = findByLoteriaIdAndHash(combinacao.getCombinacaoPK().getLoteriaId(),combinacao.getCombinacaoPK().getValue());
        if(combinacoes.size() > 0){
            return combinacoes.get(0);
        }
        em.persist(combinacao);
        return combinacao;
    }

    public Combinacao createNotExisting(Combinacao combinacao) {
        em.persist(combinacao);
        return combinacao;
    }

    public List<Combinacao> create(List<Combinacao> combinacoes) {
        Calendar inico = Calendar.getInstance();
        List<Combinacao> list = new ArrayList<>();
        for(Combinacao c : combinacoes){
            list.add(createNotExisting(c));
        }
        Calendar fim  = Calendar.getInstance();
        long tempo = fim.getTimeInMillis() - inico.getTimeInMillis();
        return list;
    }
    @Override
    protected Combinacao find(Combinacao combinacao) {
        if(combinacao.getCombinacaoPK() == null) {
            return null;
        }
        return findById(combinacao.getCombinacaoPK());
    }

    public List<Combinacao> findByLoteriaIdAndHash(Integer loteriaId, String hash){
        return  em.createNamedQuery(Combinacao.FIND_BY_LOTERIA_ID_AND_HASH,Combinacao.class)
                .setParameter("loteriaId", loteriaId)
                .setParameter("value", hash)
                .getResultList();
    }

    public List<Combinacao> findByLoteriaIdAndNumeros(Integer loteriaId, List<Integer> numeros){
        Collections.sort(numeros);
        StringBuilder value = new StringBuilder("");
        for(Integer i : numeros){
            value.append("%,").append(i).append(",");
        }
        value.append("%");

        return em.createNamedQuery(Combinacao.FIND_BY_LOTERIA_ID_AND_NUMEROS,Combinacao.class)
                .setParameter("loteriaId",loteriaId)
                .setParameter("value",value.toString())
                .getResultList();
    }
}
