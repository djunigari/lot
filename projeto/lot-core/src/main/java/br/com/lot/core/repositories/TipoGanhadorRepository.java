package br.com.lot.core.repositories;

import br.com.lot.core.model.TipoGanhador;
import br.com.lot.core.model.pks.TipoGanhadorPK;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;

@Repository
@Transactional
public class TipoGanhadorRepository extends JPARepository<TipoGanhador,TipoGanhadorPK>{
    @Inject
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return this.em;
    }

    @Override
    protected TipoGanhador find(TipoGanhador tipoGanhador) {
        if(tipoGanhador.getTipoGanhadorPK() == null){
            return null;
        }
        return findById(tipoGanhador.getTipoGanhadorPK());
    }

    public TipoGanhador findByNameAndLoteria(String nome, int loteriaId) {
        return em.createNamedQuery(TipoGanhador.FIND_BY_NAME, TipoGanhador.class)
                .setParameter("nome",nome)
                .setParameter("loteriaId",loteriaId)
                .getSingleResult();
    }
}
