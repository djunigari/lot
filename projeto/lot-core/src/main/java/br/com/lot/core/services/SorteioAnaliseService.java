package br.com.lot.core.services;

import br.com.lot.core.model.JogoAnalise;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.SorteioAnalise;
import br.com.lot.core.repositories.CombinacaoRepository;
import br.com.lot.core.repositories.JogoAnaliseRepository;
import br.com.lot.core.repositories.SorteioAnaliseRepository;
import br.com.lot.core.repositories.SorteioRepository;
import br.com.lot.core.utils.CombinacaoUtil;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class SorteioAnaliseService {
    @Inject
    private SorteioRepository sorteioRepository;
    @Inject
    private SorteioAnaliseRepository sorteioAnaliseRepository;
    @Inject
    private JogoAnaliseRepository jogoAnaliseRepository;
    @Inject
    private CombinacaoRepository combinacaoRepository;

    public List<SorteioAnalise> createAllAnalises(){
        ArrayList<SorteioAnalise> list = new ArrayList<>();
        for(Sorteio s : sorteioRepository.findAll()){
            JogoAnalise jogoAnalise = jogoAnaliseRepository.findByJogo(s.getJogo());
            if(jogoAnalise == null){
                jogoAnalise = new JogoAnalise(s.getJogo());
                jogoAnalise.setCombinacoes(new CombinacaoUtil(s.getJogo().getLoteria(), combinacaoRepository).getCombinacoes(s.getJogo()));
            }
            list.add(SorteioAnalise.newSorteioAnalise()
                    .sorteio(s)
                    .jogoAnalise(jogoAnalise)
                    .build());
        }
        return sorteioAnaliseRepository.create(list);
    }
}
