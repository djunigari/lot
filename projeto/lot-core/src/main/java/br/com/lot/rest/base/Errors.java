package br.com.lot.rest.base;

import br.com.lot.rest.exceptions.BadRequestException;
import br.com.lot.rest.exceptions.InternalException;
import br.com.lot.rest.exceptions.RestfulException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Errors implements Serializable {
    private Collection<Message> messages;

    public Errors(Collection<Message> messages) {
        this.messages = messages;
    }

    public Errors() {
    }

    public Collection<Message> getMessages() {
        if(messages == null){
            messages = new LinkedList<Message>();
        }

        return messages;
    }

    public void setMessages(Collection<Message> messages) {
        this.messages = messages;
    }

    public Errors addMessage(Long code, String message){
        Message objectMessage = new Message(code, message);
        getMessages().add(objectMessage);
        return this;
    }

    public Errors addMessage(String message, String field){
        Message objectMessage = new Message(null, message, field);
        getMessages().add(objectMessage);
        return this;
    }

    public Errors addMessage(Long code, String message, String field){
        Message objectMessage = new Message(code, message, field);
        getMessages().add(objectMessage);
        return this;
    }

    public static Errors fromException(BadRequestException e) {
        Errors errors = new Errors();
        errors.setMessages(e.getMessages());
        return errors;
    }

    public static Errors fromException(RestfulException e) {
        return new Errors().addMessage(Long.valueOf(e.getCode()), e.getMessage());
    }

    public static Errors fromException(Exception e) {
        return new Errors().addMessage(Long.valueOf(InternalException.CODE), e.getMessage());
    }
}
