package br.com.lot.core.repositories;

import br.com.lot.core.model.Combinacao;
import br.com.lot.core.model.CombinacaoAnalise;
import br.com.lot.core.model.Intervalo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class CombinacaoAnaliseRepository extends JPARepository<CombinacaoAnalise,Long>{
    @PersistenceContext
    private EntityManager em;
    @Inject
    private IntervaloRepository intervaloRepository;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    @Override
    public CombinacaoAnalise create(CombinacaoAnalise combinacaoAnalise) {
        CombinacaoAnalise find = findByCombinacao(combinacaoAnalise.getCombinacao());
        if(find != null){
            return find;
        }
        List<Intervalo> intervalos = combinacaoAnalise.getIntervalos();
        combinacaoAnalise.setIntervalos(intervaloRepository.create(intervalos));
        return super.create(combinacaoAnalise);
    }

    @Override
    protected CombinacaoAnalise find(CombinacaoAnalise combinacaoAnalise) {
        if(combinacaoAnalise.getId() == null){
            return null;
        }
        return findById(combinacaoAnalise.getId());
    }

    public CombinacaoAnalise findByCombinacao(Combinacao combinacao){
        List<CombinacaoAnalise> combinacaoAnaliseList = em.createNamedQuery(CombinacaoAnalise.FIND_BY_COMBINACAO, CombinacaoAnalise.class)
                .setParameter("value", combinacao.getCombinacaoPK().getValue())
                .setParameter("loteriaId",combinacao.getCombinacaoPK().getLoteriaId())
                .setMaxResults(1)
                .getResultList();
        if(combinacaoAnaliseList.size() == 0){
            return null;
        }
        return combinacaoAnaliseList.get(0);
    }
}
