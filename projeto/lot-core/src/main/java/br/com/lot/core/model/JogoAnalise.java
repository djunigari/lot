package br.com.lot.core.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = JogoAnalise.FIND_BY_JOGO, query = "From JogoAnalise ja where ja.jogo.id = :jogoId"),
})
@Table(name = "JOGO_ANALISE")
@SequenceGenerator(name = "SQ_JOGO_ANALISE_ID", sequenceName = "SQ_JOGO_ANALISE_ID", allocationSize = 1)
public class JogoAnalise {
    public static final String FIND_BY_JOGO = "JogoAnalise.findByJogo";

    @Id@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_JOGO_ANALISE_ID")
    @Column(name = "JOGO_ANALISE_ID")
    private Long id;
    @OneToOne
    @NotNull
    @JoinColumn(name = "JOGO_ID")
    private Jogo jogo;
    @Column(name = "SOMA")
    private Integer soma;
    @Column(name = "QTDE_PAR")
    private Integer qtdePar;
    @Column(name = "QTDE_IMPAR")
    private Integer qtdeImpar;
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name="JOGO_ANALISE_HAS_COMBINACOES",
            joinColumns=@JoinColumn(name="JOGO_ANALISE_ID", referencedColumnName="JOGO_ANALISE_ID"),
            inverseJoinColumns={@JoinColumn(name="COMBINACAO_VALUE", referencedColumnName="VALUE"),
                    @JoinColumn(name="COMBINACAO_LOTERIA_ID", referencedColumnName="LOTERIA_ID"),
            })
    private List<Combinacao> combinacoes;
    @Column(name = "MAIOR_NUMERO")
    private Integer maiorNumero;
    @Column(name = "MENOR_NUMERO")
    private Integer menorNumero;
    @Column(name = "MEDIO_NUMERO")
    private Double medioNumero;

    public JogoAnalise(){

    }

    public JogoAnalise(Jogo jogo){
        this.jogo = jogo;
        processJogo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Jogo getJogo() {
        return jogo;
    }

    public Integer getSoma() {
        return soma;
    }

    public Integer getQtdePar() {
        return qtdePar;
    }

    public Integer getQtdeImpar() {
        return qtdeImpar;
    }

    public List<Combinacao> getCombinacoes() {
        return combinacoes;
    }

    public void setCombinacoes(List<Combinacao> combinacoes) {
        this.combinacoes = combinacoes;
    }

    public Integer getMaiorNumero() {
        return maiorNumero;
    }

    public Integer getMenorNumero() {
        return menorNumero;
    }

    public Double getMedioNumero() {
        return medioNumero;
    }


    private void processJogo(){
        this.soma = 0;
        this.qtdePar = 0;
        this.qtdeImpar = 0;
        this.maiorNumero = jogo.getDezenas().get(0).getDezenaPK().getNumero();
        this.menorNumero = jogo.getDezenas().get(0).getDezenaPK().getNumero();
        for(Dezena d : jogo.getDezenas()){
            Integer numero = d.getDezenaPK().getNumero();
            soma += numero;
            if(numero % 2 == 0){
                qtdePar++;
            }else{
                qtdeImpar++;
            }
            if(maiorNumero < numero){
                maiorNumero = numero;
            }
            if(menorNumero > numero){
                menorNumero = numero;
            }
        }
        medioNumero = soma / jogo.getLoteria().getQuantidadeDeDezenas().doubleValue();
    }
}
