package br.com.lot.rest.converters.utils;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named
public class HtmlConverterConfig {
	private String nomeLoteria;
	private Map<String,Integer> columns;

	public String getNomeLoteria() {
		return nomeLoteria;
	}

	public void setNomeLoteria(String nomeLoteria) {
		this.nomeLoteria = nomeLoteria;
	}

	public void setColumnsNames(Element row){
		Map<String,Integer> columns = new HashMap<>();
		Elements titles = row.select("th");
		for(int i =0; i < titles.size(); i++){
			columns.put(titles.get(i).text().toUpperCase(),i);
		}
		this.columns = columns;
	}
	
	public String getColumnValue(Elements columnsValues, String name){
		return columnsValues.get(columns.get(name.toUpperCase())).text();
	}
}
