package br.com.lot.rest.util;

public class PaginationUtils {
    public static int first(int page, int pageSize){
        return (page - 1) * pageSize;
    }
}
