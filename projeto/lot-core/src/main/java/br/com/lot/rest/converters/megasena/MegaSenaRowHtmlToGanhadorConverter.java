package br.com.lot.rest.converters.megasena;

import br.com.lot.core.model.Ganhador;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.TipoGanhador;
import br.com.lot.core.repositories.LoteriaRepository;
import br.com.lot.core.repositories.TipoGanhadorRepository;
import br.com.lot.rest.converters.BaseConverter;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static br.com.lot.core.model.Ganhador.newGanhador;

@Named
public class MegaSenaRowHtmlToGanhadorConverter extends BaseConverter<Element,List<Ganhador>> {
    private Elements columns;
    private Loteria loteria;
    @Inject
    private HtmlConverterConfig converterUtil;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private TipoGanhadorRepository tipoGanhadorRepository;

    @Override
    public List<Ganhador> convert(Element tr) {
        columns = tr.select("td");

        List<Ganhador> ganhadores = new ArrayList<>();

        for(int i = 4 ; i <= loteria.getQuantidadeDeDezenas() ; i++){
            ganhadores.add(getGanhadores(i));
        }

        return ganhadores;
    }

    private Ganhador getGanhadores(int quantidadeDeAcertos){
        String nomeTipoGanhador = "Ganhadores_" + TypeGanhadorEnum.getByNumber(quantidadeDeAcertos).getName();
        TipoGanhador tipoGanhador = tipoGanhadorRepository
                .findByNameAndLoteria(nomeTipoGanhador, loteria.getId());

        return newGanhador()
                .tipoDeGanhador(tipoGanhador)
                .quantidadeDeGanhadores(new Integer(getString(nomeTipoGanhador)))
                .premioParaCada(new BigDecimal(getDecimal("Rateio_" + TypeGanhadorEnum.getByNumber(quantidadeDeAcertos).getName())))
                .build();
    }

    public String getString(String columnName){
        return converterUtil.getColumnValue(columns, columnName);
    }

    public String getDecimal(String columnName){
        return converterUtil.getColumnValue(columns, columnName).replace(".","").replace(",", ".");
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }

    private enum TypeGanhadorEnum{
        SENA("Sena",6),QUINA("Quina",5),QUADRA("Quadra",4);

        String name;
        int number;
        TypeGanhadorEnum(String name, int number){
            this.name = name;
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public static TypeGanhadorEnum getByNumber(int number){
            for(TypeGanhadorEnum  t :TypeGanhadorEnum.values()){
                if(t.getNumber() == number){
                    return t;
                }
            }
            return null;
        }
    }

}
