package br.com.lot.rest.resources;

import br.com.lot.rest.base.Item;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonRootName("ganhador")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GanhadorResource extends Item<Long> {
    @JsonProperty("tipo-ganhador")
    private TipoGanhadorResource tipoDeGanhador;
    @JsonProperty("quantidade-de-ganhadores")
    private Integer quantidadeDeGanhadores;
    @JsonProperty("premio-para-cada")
    private BigDecimal premioParaCada;

    public TipoGanhadorResource getTipoDeGanhador() {
        return tipoDeGanhador;
    }

    public void setTipoDeGanhador(TipoGanhadorResource tipoDeGanhador) {
        this.tipoDeGanhador = tipoDeGanhador;
    }

    public Integer getQuantidadeDeGanhadores() {
        return quantidadeDeGanhadores;
    }

    public void setQuantidadeDeGanhadores(Integer quantidadeDeGanhadores) {
        this.quantidadeDeGanhadores = quantidadeDeGanhadores;
    }

    public BigDecimal getPremioParaCada() {
        return premioParaCada;
    }

    public void setPremioParaCada(BigDecimal premioParaCada) {
        this.premioParaCada = premioParaCada;
    }
}
