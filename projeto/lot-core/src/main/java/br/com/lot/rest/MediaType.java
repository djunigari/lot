package br.com.lot.rest;

public class MediaType extends javax.ws.rs.core.MediaType {
    public final static String APPLICATION_RESOURCE_JSON = "application/vnd.resource+json;charset=UTF-8";
}
