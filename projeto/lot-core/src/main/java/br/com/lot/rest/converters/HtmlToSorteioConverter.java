package br.com.lot.rest.converters;


import br.com.lot.core.model.Sorteio;
import br.com.lot.core.producers.qualifiers.ConverterLiteral;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import br.com.lot.sis.Factory;
import br.com.lot.sis.Instance;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
public class HtmlToSorteioConverter extends BaseConverter<String,List<Sorteio>> {
	@Inject
	private HtmlConverterConfig htmlConverterConfig;

	@Factory
	private Instance<BaseConverter<Element,Sorteio>> sorteioConverterInstance;

	@Override
	public List<Sorteio> convert(String html) {
		BaseConverter converter = sorteioConverterInstance
				.select(ConverterLiteral.converter(htmlConverterConfig.getNomeLoteria().concat("ToSorteio"))).get();

		Elements rows = Jsoup.parse(html)
				.select("table")
				.get(0)
				.select("tr");

		htmlConverterConfig.setColumnsNames(rows.get(0));

		List<Sorteio> sorteios = new ArrayList<Sorteio>();
		for(int i = 1 ; i < rows.size(); i++){
			Sorteio sorteio = (Sorteio)converter.convert(rows.get(i));
			if(sorteio == null){
				continue;
			}
			sorteios.add(sorteio);
		}
		return sorteios;
	}
}
