package br.com.lot.core.model;

import br.com.lot.core.model.pks.TipoGanhadorPK;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
		@NamedQuery(name = TipoGanhador.FIND_BY_NAME, query = "From TipoGanhador t where t.tipoGanhadorPK.nome = :nome and t.tipoGanhadorPK.loteriaId = :loteriaId"),
})
@Table(name = "TIPOGANHADOR")
public class TipoGanhador implements Serializable{
	public static final String FIND_BY_NAME = "TipoGanhador.findByName";

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TipoGanhadorPK tipoGanhadorPK;
	@Column(name="QUANTIDADEDEACERTOS")
	private Integer quantidadeDeAcertos;

	public TipoGanhador(){}
	
	private TipoGanhador(Builder builder) {
		this.tipoGanhadorPK = builder.tipoGanhadorPK;
		this.quantidadeDeAcertos = builder.quantidadeDeAcertos;
	}

	public static Builder newTipoGanhador() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	public TipoGanhadorPK getTipoGanhadorPK() {
		return tipoGanhadorPK;
	}
	public void setTipoGanhadorPK(TipoGanhadorPK tipoGanhadorPK) {
		this.tipoGanhadorPK = tipoGanhadorPK;
	}
	public Integer getQuantidadeDeAcertos() {
		return quantidadeDeAcertos;
	}
	public void setQuantidadeDeAcertos(Integer quantidadeDeAcertos) {
		this.quantidadeDeAcertos = quantidadeDeAcertos;
	}


	public static final class Builder {
		private TipoGanhadorPK tipoGanhadorPK;
		private Integer quantidadeDeAcertos;

		private Builder() {
		}

		public TipoGanhador build() {
			return new TipoGanhador(this);
		}

		public Builder tipoGanhadorPK(TipoGanhadorPK tipoGanhadorPK) {
			this.tipoGanhadorPK = tipoGanhadorPK;
			return this;
		}

		public Builder quantidadeDeAcertos(Integer quantidadeDeAcertos) {
			this.quantidadeDeAcertos = quantidadeDeAcertos;
			return this;
		}
	}
}
