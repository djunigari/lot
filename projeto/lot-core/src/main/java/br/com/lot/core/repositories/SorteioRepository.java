package br.com.lot.core.repositories;

import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.SorteioAnalise;
import br.com.lot.core.model.pks.CombinacaoPK;
import br.com.lot.core.model.pks.SorteioPK;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class SorteioRepository extends JPARepository<Sorteio,SorteioPK>{

    @Inject
    private JogoRepository jogoRepository;

    @Inject
    private GanhadorRepository ganhadoresRepository;

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    @Override
    protected Sorteio find(Sorteio sorteio) {
        if(sorteio.getSorteioPK() == null){
            return null;
        }
        return findById(sorteio.getSorteioPK());
    }

    @Override
    public Sorteio create(Sorteio sorteio) {
        sorteio.setJogo(jogoRepository.create(sorteio.getJogo()));
        sorteio.setGanhadores(ganhadoresRepository.create(sorteio.getGanhadores()));
        return super.create(sorteio);
    }

    public List<Sorteio> create(List<Sorteio> sorteios){
        List<Sorteio> result = new ArrayList<>();
        for(Sorteio s : sorteios) {
            result.add(create(s));
        }
        return result;
    }

    public Sorteio findLastSorteioByCombinacaoId(CombinacaoPK pk) {
        try {
            return em.createNamedQuery(SorteioAnalise.FIND_LAST_SORTEIO_BY_COMBINACAO_ID, Sorteio.class)
                    .setParameter("value", pk.getValue())
                    .setParameter("loteriaId",pk.getLoteriaId())
                    .setMaxResults(1)
                    .getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }
}
