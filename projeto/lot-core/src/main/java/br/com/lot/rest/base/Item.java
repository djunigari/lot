package br.com.lot.rest.base;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Item<K> implements Serializable {
    @JsonProperty("id")
    protected K id;
    @JsonProperty("uri")
    protected String uri;
    @JsonProperty("links")
    protected Map<String, Link> links;
    @JsonIgnore
    protected String resourcePath;

    public Item() {
    }

    public Item(K id, String resourcePath) {
        if (id == null) {
            return;
        }

        this.id = id;
        this.resourcePath = resourcePath;

        buildUri();
    }

    protected String getResourcePath() {
        return resourcePath;
    }

    protected void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public K getId() {
        return id;
    }

    public void setId(K id) {
        this.id = id;
    }

    public Map<String, Link> getLinks() {
        return links;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    protected void buildUri() {
        this.uri = resourcePath + "/" + id;
    }

    public Item<K> addLink(String rel, String href) {
        if (this.links == null) {
            this.links = new HashMap<String, Link>();
        }

        this.links.put(rel, new Link(href));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Item))
            return false;

        Item item = (Item) o;

        if (id != null ? !id.equals(item.id) : item.id != null)
            return false;
        if (resourcePath != null ? !resourcePath.equals(item.resourcePath) : item.resourcePath != null)
            return false;
        if (uri != null ? !uri.equals(item.uri) : item.uri != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (uri != null ? uri.hashCode() : 0);
        result = 31 * result + (resourcePath != null ? resourcePath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", uri='" + uri + '\'' + ", resourcePath='" + resourcePath + '\'' + '}';
    }
}