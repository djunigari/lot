package br.com.lot.rest.converters;

import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.pks.DezenaPK;
import br.com.lot.core.repositories.LoteriaRepository;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static br.com.lot.core.model.Dezena.newDezena;
import static br.com.lot.core.model.Jogo.newJogo;

@Repository
public class LotofacilRowHtmlToJogoConverter extends BaseConverter<Element,Jogo> {

    private Elements columns;
    @Inject
    private HtmlConverterConfig converterUtil;
    @Inject
    private LoteriaRepository loteriaRepository;
    private Loteria loteria;

    @Override
    public Jogo convert(Element tr) {
        columns = tr.select("td");
        List<Dezena> dezenas = new ArrayList<>();
        for(int i = 1; i <= loteria.getQuantidadeDeDezenas(); i++){
            dezenas.add(getDezena(i));
        }

        return  newJogo()
                .loteria(loteria)
                .dezenas(dezenas)
                .build();
    }

    private Dezena getDezena(Integer i){
        return newDezena()
                .dezenaPK(new DezenaPK(new Integer(getString("Bola" + i)),i, loteria.getId()))
                .build();
    }

    public String getString(String columnName){
        return converterUtil.getColumnValue(columns, columnName);
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }
}
