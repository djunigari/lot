package br.com.lot.rest.converters.megasena;

import br.com.lot.core.model.Ganhador;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.core.producers.qualifiers.Converter;
import br.com.lot.core.repositories.LoteriaRepository;
import br.com.lot.rest.converters.BaseConverter;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static br.com.lot.core.model.Sorteio.newSorteio;

@Named
@Converter("MegaSenaToSorteio")
public class MegaSenaRowHtmlToSorteioConverter extends BaseConverter<Element,Sorteio> {
    private static final int QUANTIDADE_DE_COLUNAS = 21;

    private Elements columns;
    @Inject
    private HtmlConverterConfig converterUtil;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private MegaSenaRowHtmlToJogoConverter megaSenaRowHtmlToJogoConverter;
    @Inject
    private MegaSenaRowHtmlToGanhadorConverter megaSenaRowHtmlToGanhadorConverter;

    @Override
    public Sorteio convert(Element tr) {
        Loteria loteria = loteriaRepository.findByNome(converterUtil.getNomeLoteria());

        columns = tr.select("td");

        if(columns.size() != QUANTIDADE_DE_COLUNAS){
            return null;
        }
        Integer concurso = new Integer(getString("Concurso"));
        Date dataSorteio;
        try {
            dataSorteio = new SimpleDateFormat("dd/MM/yyyy").parse(getString("Data Sorteio"));
        } catch (ParseException e) {
            e.printStackTrace();
            dataSorteio = null;
        }
        String cidade = getString("Cidade");
        String uf = getString("UF");
        BigDecimal arrecadacaoTotal = new BigDecimal(getDecimal("Arrecadacao_Total"));
        BigDecimal valorAcumulado = new BigDecimal(getDecimal("Valor_Acumulado"));
        BigDecimal estimativaPremio = new BigDecimal(getDecimal("Estimativa_Prêmio"));
        BigDecimal valorAcumuladoEspecial = new BigDecimal(getDecimal("Acumulado_Mega_da_Virada"));
        SorteioPK sorteioPK = new SorteioPK(concurso,loteria.getId());
        Jogo jogo = megaSenaRowHtmlToJogoConverter.convert(tr);
        List<Ganhador> ganhadores = megaSenaRowHtmlToGanhadorConverter.convert(tr);

        return newSorteio()
                .sorteioPK(sorteioPK)
                .dataSorteio(dataSorteio)
                .jogo(jogo)
                .arrecadacaoTotal(arrecadacaoTotal)
                .cidade(cidade)
                .uf(uf)
                .ganhadores(ganhadores)
                .valorAcumulado(valorAcumulado)
                .estimativaPremio(estimativaPremio)
                .valorAcumuladoEspecial(valorAcumuladoEspecial)
                .build();
    }

    public String getString(String columnName){
        return converterUtil.getColumnValue(columns, columnName);
    }

    public String getDecimal(String columnName){
        return converterUtil.getColumnValue(columns, columnName).replace(".","").replace(",", ".");
    }
}
