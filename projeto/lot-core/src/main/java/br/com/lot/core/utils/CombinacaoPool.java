package br.com.lot.core.utils;

import br.com.lot.core.model.Combinacao;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.repositories.CombinacaoRepository;

import java.util.HashMap;
import java.util.TreeSet;

public class CombinacaoPool {
    private CombinacaoRepository repository;

    public HashMap<Integer,TreeSet<Combinacao>> pools;

    public CombinacaoPool(Loteria loteria, CombinacaoRepository repository){
        this.pools = new HashMap<>();
        for(int i = 1; i <= loteria.getQuantidadeDeDezenas();i++){
            pools.put(i, new TreeSet<Combinacao>());
        }
        this.repository = repository;
    }

    public Combinacao insert(Combinacao combinacao){
        TreeSet<Combinacao> tree = pools.get(combinacao.getQtdeNumeros());
        if(tree.add(combinacao)){
            repository.createNotExisting(combinacao);
            return combinacao;
        }
        return tree.ceiling(combinacao);
    }
}
