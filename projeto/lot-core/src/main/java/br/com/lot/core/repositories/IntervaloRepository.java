package br.com.lot.core.repositories;

import br.com.lot.core.model.Intervalo;
import br.com.lot.core.model.Sorteio;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class IntervaloRepository  extends  JPARepository<Intervalo,Long>{
    @Inject
    private EntityManager em;
    @Override
    protected EntityManager entityManager() {
        return em;
    }

    @Override
    protected Intervalo find(Intervalo intervalo) {
        return findByFirstAndLastSorteio(intervalo.getFirstSorteio(), intervalo.getLastSorteio());
    }

    public List<Intervalo> create(List<Intervalo> intervalos) {
        List<Intervalo> list = new ArrayList<>();
        for(Intervalo i : intervalos){
            list.add(create(i));
        }
        return list;
    }

    public Intervalo findByFirstAndLastSorteio(Sorteio first, Sorteio last){

        List<Intervalo> intervalos = em.createNamedQuery(Intervalo.FIND_BY_FIRST_AND_LAST_SORTEIO, Intervalo.class)
                .setParameter("first", first)
                .setParameter("last", last)
                .getResultList();
        if(intervalos.size() == 0){
            return null;
        }
        return intervalos.get(0);
    }
}
