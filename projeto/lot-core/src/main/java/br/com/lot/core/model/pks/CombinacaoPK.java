package br.com.lot.core.model.pks;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CombinacaoPK implements Serializable {

    @Column(name = "VALUE")
    private String value;
    @Column(name = "LOTERIA_ID")
    private Integer loteriaId;

    public CombinacaoPK(){}
    public CombinacaoPK(Integer loteriaId, String value){
        this.loteriaId = loteriaId;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public Integer getLoteriaId() {
        return loteriaId;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CombinacaoPK){
            CombinacaoPK pk = (CombinacaoPK)obj;
            if(!pk.getValue().equals(value)){
                return false;
            }
            if(!pk.getLoteriaId().equals(loteriaId)){
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ("l"+loteriaId+value).hashCode();
    }
}
