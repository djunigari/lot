package br.com.lot.rest.base;

import java.util.Collection;

public class ResourceBuilder<T extends Item<?>> {
private T item;
private String uri;
private ItemsBuilder items;
private Errors errors;

public static <T extends Item<?>> ResourceBuilder<T> builder(){
        return new ResourceBuilder<T>();
        }

public ResourceBuilder<T> item(T item) {
        this.item = item;
        return this;
        }

public ResourceBuilder<T> uri(String uri) {
        this.uri = uri;
        return this;
        }

public ItemsBuilder items() {
        if (items == null) {
        items = new ItemsBuilder(this);
        }
        return items;
        }

public ResourceBuilder<T> error(String message) {
        return error(null, message, null);
        }

public ResourceBuilder<T> error(Long code, String message, String field) {

        if (errors == null) {
        errors = new Errors();
        }

        errors.addMessage(code, message, field);

        return this;
        }

public Resource<T> build() {
        if (errors != null) {
        return new Resource<T>(uri, errors);
        }

        if (items == null) {
        return new Resource<T>(item);
        }

        if (items.max() == 0 && items.from() == 0) {
        return new Resource<T>(uri, (Collection<T>) items.items());
        }

        return new Resource<T>(uri, items.resource(), (Collection<T>) items.items(), items.totalSize(), items.from(), items.max());
        }

public static class ItemsBuilder {
    private String resource;
    private Collection<?> items;
    private long totalSize;
    private ResourceBuilder builder;
    private int page;
    private int pageSize;
    private Collection<Link> links;

    public ItemsBuilder(ResourceBuilder builder) {
        this.builder = builder;
    }

    public ItemsBuilder items(Collection<?> items) {
        this.items = items;
        return this;
    }

    public ItemsBuilder totalSize(long totalSize) {
        this.totalSize = totalSize;
        return this;
    }

    public ItemsBuilder page(int page) {
        this.page = page;
        return this;
    }

    public ItemsBuilder pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public ItemsBuilder resource(String resource) {
        this.resource = resource;
        return this;
    }

    public ResourceBuilder back() {
        return builder;
    }

    private String resource() {
        return resource;
    }

    private Collection<?> items() {
        return items;
    }

    private long totalSize() {
        return totalSize;
    }

    private int from() {
        return page;
    }

    private int max() {
        return pageSize;
    }

    private Collection<Link> links() {
        return links;
    }
}
}
