package br.com.lot.core.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = CombinacaoAnalise.FIND_BY_COMBINACAO, query = "From CombinacaoAnalise ca where ca.combinacao.combinacaoPK.value = :value and ca.combinacao.combinacaoPK.loteriaId = :loteriaId"),
})
@Table(name="COMBINACAOANALISE")
public class CombinacaoAnalise {
    public static final String FIND_BY_COMBINACAO = "CombinacaoAnalise.findByCombinacao";

    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    @NotNull
    private Combinacao combinacao;
    private Integer soma;
    private Integer qtdePar;
    private Integer qtdeImpar;
    private Integer maiorNumero;
    private Integer menorNumero;
    private Double medioNumero;
    @ManyToMany
    private List<Intervalo> intervalos;

    public CombinacaoAnalise(){

    }

    public CombinacaoAnalise(Combinacao combinacao){
        this.combinacao = combinacao;
        processCombinacao();
    }

    public Long getId() {
        return id;
    }

    public Combinacao getCombinacao() {
        return combinacao;
    }

    public Integer getSoma() {
        return soma;
    }

    public Integer getQtdePar() {
        return qtdePar;
    }

    public Integer getQtdeImpar() {
        return qtdeImpar;
    }

    public Integer getMaiorNumero() {
        return maiorNumero;
    }

    public Integer getMenorNumero() {
        return menorNumero;
    }

    public Double getMedioNumero() {
        return medioNumero;
    }

    public List<Intervalo> getIntervalos() {
        return intervalos;
    }

    public void setIntervalos(List<Intervalo> intervalos) {
        this.intervalos = intervalos;
    }

    public CombinacaoAnalise addIntervalo(Intervalo intervalo){
        for(Intervalo i : intervalos){
            if(i.equals(intervalo)){
                throw new IllegalArgumentException();
            }
        }
        this.intervalos.add(intervalo);
        return this;
    }

    private void processCombinacao(){
        this.soma = 0;
        this.qtdePar = 0;
        this.qtdeImpar = 0;
        this.maiorNumero = combinacao.getNumeros().get(0);
        this.menorNumero = combinacao.getNumeros().get(0);
        for(Integer numero : combinacao.getNumeros()){
            soma += numero;
            if(numero % 2 == 0){
                qtdePar++;
            }else{
                qtdeImpar++;
            }
            if(maiorNumero < numero){
                maiorNumero = numero;
            }
            if(menorNumero > numero){
                menorNumero = numero;
            }
        }

        medioNumero = soma / combinacao.getQtdeNumeros().doubleValue();
    }
}
