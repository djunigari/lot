package br.com.lot.rest.exceptions;

public class RestfulException extends RuntimeException {
    private static final long serialVersionUID = 6340181351898416399L;
    private Integer code;
    private Integer httpStatus;

    public RestfulException(String message, Integer code, Integer httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public Integer getCode() {
        return this.code;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

}
