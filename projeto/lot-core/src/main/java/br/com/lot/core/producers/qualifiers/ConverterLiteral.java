package br.com.lot.core.producers.qualifiers;

import br.com.lot.sis.QualifierLiteral;

public class ConverterLiteral extends QualifierLiteral<Converter> implements Converter {
    private String value;

    private ConverterLiteral(String value){
        this.value = value;
    }

    public static ConverterLiteral converter(String value){
        return new ConverterLiteral(value);
    }

    @Override
    public String value() {
        return value;
    }
}
