package br.com.lot.core.model.pks;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TipoGanhadorPK implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nome;
	@Column(name = "LOTERIAID")
	private Integer loteriaId;

	public TipoGanhadorPK(String nome,Integer loteriaId ){
		this.nome = nome;
		this.loteriaId = loteriaId;
	}
	
	public TipoGanhadorPK() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getLoteriaId() {
		return loteriaId;
	}

	public void setLoteriaId(Integer loteriaId) {
		this.loteriaId = loteriaId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TipoGanhadorPK) {
			TipoGanhadorPK pk = (TipoGanhadorPK) obj;
			if (pk.getNome().equals(nome)) {
				return false;
			}
			if (pk.getLoteriaId().equals(loteriaId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return nome.hashCode() + loteriaId.hashCode();
	}
}
