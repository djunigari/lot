package br.com.lot.rest.util;

import java.util.Scanner;


public class FileUtil {
    public  String fetchFile(String file) {
//    	Path path = Paths.get("src/main/resources/".concat(file));
//    	try {
//    		StringBuilder sb = new StringBuilder("");
//    		Stream<String> linhas = Files.lines(path,StandardCharsets.ISO_8859_1);
//			linhas.forEach(l -> sb.append(l));
//			linhas.close();
//			return sb.toString();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return null;
//		}
		StringBuilder sb = new StringBuilder();
		Scanner in = new Scanner(this.getClass().getResourceAsStream(file),"UTF-8");
		while(in.hasNext()){
			sb.append(in.nextLine());
		}
		in.close();
		return sb.toString();
    }
}
