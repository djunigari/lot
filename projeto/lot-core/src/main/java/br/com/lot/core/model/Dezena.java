package br.com.lot.core.model;

import br.com.lot.core.model.pks.DezenaPK;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DEZENA")
public class Dezena implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DezenaPK dezenaPK;

	public Dezena(){
	}

	private Dezena(Builder builder) {
		this.dezenaPK = builder.dezenaPK;
	}

	public static Builder newDezena() {
		return new Builder();
	}

	public DezenaPK getDezenaPK() {
		return dezenaPK;
	}
	public void setDezenaPK(DezenaPK dezenaPK) {
		this.dezenaPK = dezenaPK;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Dezena)){
			return false;
		}
		return dezenaPK.equals(((Dezena)obj).getDezenaPK());
	}

	public static final class Builder {
		private DezenaPK dezenaPK;

		private Builder() {
		}

		public Dezena build() {
			return new Dezena(this);
		}

		public Builder dezenaPK(DezenaPK dezenaPK) {
			this.dezenaPK = dezenaPK;
			return this;
		}
	}
}
