package br.com.lot.rest.converters;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseConverter<T, R>{

	public abstract R convert(T from);
	
	public List<R> convert(List<T> collection) {
		if (collection == null) {
			return null;
		}
		
		List<R> list = new LinkedList<R>();
		for (T object : collection) {
			list.add(convert(object));
		}
		
		return list;
	}
}
