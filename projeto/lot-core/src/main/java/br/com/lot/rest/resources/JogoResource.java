package br.com.lot.rest.resources;

import br.com.lot.rest.base.Item;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonRootName("jogo")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JogoResource extends Item<Long> {
    @JsonProperty("concurso")
    private Integer concurso;
    @JsonProperty("loteria")
    private LoteriaResource loteria;
    @JsonProperty("dezenas")
    private List<DezenaResource> dezenas;

    public Integer getConcurso() {
        return concurso;
    }

    public void setConcurso(Integer concurso) {
        this.concurso = concurso;
    }

    public LoteriaResource getLoteria() {
        return loteria;
    }

    public void setLoteria(LoteriaResource loteria) {
        this.loteria = loteria;
    }

    public List<DezenaResource> getDezenas() {
        return dezenas;
    }

    public void setDezenas(List<DezenaResource> dezenas) {
        this.dezenas = dezenas;
    }
}
