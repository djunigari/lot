package br.com.lot.core.repositories;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@Transactional
public abstract class JPARepository<T, K> {
	public T create(T t) {
		T entity = find(t);
		if(entity != null){
			return entity;
		}
		entityManager().persist(t);
		entityManager().flush();
		entityManager().refresh(t);
		return t;
	}

	protected abstract T find(T t);

	public T findById(K id) {
		return entityManager().find(getTypeClass(), id);
	}

	public void delete(K id) {
		T ref = entityManager().getReference(getTypeClass(), id);
		entityManager().remove(ref);
	}

	public T update(T t) {
        T merge = entityManager().merge(t);
        entityManager().flush();
        return merge;
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String namedQueryName) {
		return entityManager().createNamedQuery(namedQueryName).getResultList();
	}

	public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String queryName, int resultLimit) {
		return entityManager().createNamedQuery(queryName).setMaxResults(resultLimit).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByNativeQuery(String sql) {
		return entityManager().createNativeQuery(sql, getTypeClass()).getResultList();
	}

	public List<T> findAll() {
		Class<T> clazz = getTypeClass();
		return entityManager().createQuery("SELECT e FROM " + clazz.getSimpleName() + " e", clazz).getResultList();
	}

    public Long size() {
        Class<T> clazz = getTypeClass();
        return entityManager().createQuery("SELECT count(e) FROM " + clazz.getSimpleName() +" e", Long.class).getSingleResult();
    }

    public boolean contains(K id) {
        return findById(id) != null;
    }

	public List<T> findAll(int first, int max) {
		Class<T> clazz = getTypeClass();

		TypedQuery<T> query = entityManager().createQuery("FROM " + clazz.getSimpleName(), clazz);

		query.setFirstResult(first);
		query.setMaxResults(max);
		
		return query.getResultList();
	}
	
	public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
        TypedQuery<T> query = entityManager().createNamedQuery(namedQueryName, getTypeClass());
		if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

	/**
	 * MAGIC: DO NOT TOUCH
	 */
	@SuppressWarnings("unchecked")
	private Class<T> getTypeClass() {
		if(this.getClass().getGenericSuperclass() instanceof ParameterizedType){
			return (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		}

		if(((Class)this.getClass().getGenericSuperclass()).getGenericSuperclass() instanceof ParameterizedType){
			return (Class) ((ParameterizedType) ((Class)this.getClass().getGenericSuperclass()).getGenericSuperclass()).getActualTypeArguments()[0];
		}

		return null;
	}

	public static class QueryParameter {

		private Map<String, Object> parameters = null;

		private QueryParameter(String name, Object value) {
			this.parameters = new HashMap<String, Object>();
			this.parameters.put(name, value);
		}

		public static QueryParameter with(String name, Object value) {
			return new QueryParameter(name, value);
		}

		public QueryParameter and(String name, Object value) {
			this.parameters.put(name, value);
			return this;
		}

		public Map<String, Object> parameters() {
			return this.parameters;
		}
	}

	protected abstract EntityManager entityManager();
}
