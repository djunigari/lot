package br.com.lot.core.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
        @NamedQuery(name = SorteioAnalise.FIND_BY_SORTEIO, query = "From SorteioAnalise sa where sa.sorteio = :sorteio "),
        @NamedQuery(name = SorteioAnalise.FIND_LAST_SORTEIO_BY_COMBINACAO_ID, query = "Select sa.sorteio From SorteioAnalise sa join sa.jogoAnalise ja JOIN ja.combinacoes c where c.combinacaoPK.loteriaId = :loteriaId and  c.combinacaoPK.value = :value order by sa.sorteio.sorteioPK.concurso desc"),
})
@Table(name="SORTEIOANALISE")
public class SorteioAnalise {
    public static final String FIND_BY_SORTEIO = "SorteioAnalise.findBySorteio";
    public static final String FIND_LAST_SORTEIO_BY_COMBINACAO_ID = "SorteioAnalise.findLastSorteioByCombinacaoId";

    @Id@GeneratedValue
    private Integer id;
    @OneToOne
    @NotNull
    private Sorteio sorteio;
    @ManyToOne
    @NotNull
    private JogoAnalise jogoAnalise;

    public SorteioAnalise(){

    }

    private SorteioAnalise(Builder builder) {
        this.sorteio = builder.sorteio;
        this.jogoAnalise = builder.jogoAnalise;
    }

    public static Builder newSorteioAnalise() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sorteio getSorteio() {
        return sorteio;
    }

    public void setSorteio(Sorteio sorteio) {
        this.sorteio = sorteio;
    }

    public JogoAnalise getJogoAnalise() {
        return jogoAnalise;
    }

    public void setJogoAnalise(JogoAnalise jogoAnalise) {
        this.jogoAnalise = jogoAnalise;
    }

    public static final class Builder {
        private Sorteio sorteio;
        private JogoAnalise jogoAnalise;

        private Builder() {
        }

        public SorteioAnalise build() {
            return new SorteioAnalise(this);
        }

        public Builder sorteio(Sorteio sorteio) {
            this.sorteio = sorteio;
            return this;
        }

        public Builder jogoAnalise(JogoAnalise jogoAnalise) {
            this.jogoAnalise = jogoAnalise;
            return this;
        }
    }
}
