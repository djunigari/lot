package br.com.lot.core.model;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = Intervalo.FIND_BY_FIRST_AND_LAST_SORTEIO, query = "From Intervalo i where i.firstSorteio = :first and i.lastSorteio = :last"),
})
public class Intervalo {
    public static final String FIND_BY_FIRST_AND_LAST_SORTEIO = "findByFirstAndLastSorteio";
    @Id
    @GeneratedValue
    public Long id;
    @ManyToOne
    public Sorteio firstSorteio;
    @ManyToOne
    public Sorteio lastSorteio;
    public Integer value;

    public Intervalo(){

    }

    public Intervalo(Sorteio firstSorteio, Sorteio lastSorteio) {
        this.firstSorteio = firstSorteio;
        this.lastSorteio = lastSorteio;
        this.value = lastSorteio.getSorteioPK().getConcurso() - lastSorteio.getSorteioPK().getConcurso();
    }

    public Long getId() {
        return id;
    }

    public Sorteio getFirstSorteio() {
        return firstSorteio;
    }

    public Sorteio getLastSorteio() {
        return lastSorteio;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof  Intervalo)){
            return false;
        }
        Intervalo intervalo = (Intervalo)obj;
        if(!firstSorteio.equals(intervalo.getFirstSorteio())){
            return false;
        }
        if(!lastSorteio.equals(intervalo.getLastSorteio())){
            return  false;
        }
        return true;
    }
}
