package br.com.lot.rest.converters;

import br.com.lot.core.model.Ganhador;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.TipoGanhador;
import br.com.lot.core.repositories.LoteriaRepository;
import br.com.lot.core.repositories.TipoGanhadorRepository;
import br.com.lot.rest.converters.utils.HtmlConverterConfig;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static br.com.lot.core.model.Ganhador.newGanhador;

@Named
public class LotofacilRowHtmlToGanhadorConverter extends BaseConverter<Element,List<Ganhador>> {
    private Elements columns;
    private Loteria loteria;
    @Inject
    private HtmlConverterConfig converterUtil;
    @Inject
    private LoteriaRepository loteriaRepository;
    @Inject
    private TipoGanhadorRepository tipoGanhadorRepository;

    @Override
    public List<Ganhador> convert(Element tr) {
        columns = tr.select("td");

        List<Ganhador> ganhadores = new ArrayList<>();

        for(int i = 11 ; i <= loteria.getQuantidadeDeDezenas() ; i++){
            ganhadores.add(getGanhadores(i));
        }

        return ganhadores;
    }

    private Ganhador getGanhadores(int quantidadeDeAcertos){
        String nomeTipoGanhador = "Ganhadores_" + quantidadeDeAcertos + "_Números";
        TipoGanhador tipoGanhador = tipoGanhadorRepository
                .findByNameAndLoteria(nomeTipoGanhador, loteria.getId());

        return newGanhador()
                .tipoDeGanhador(tipoGanhador)
                .quantidadeDeGanhadores(new Integer(getString(nomeTipoGanhador)))
                .premioParaCada(new BigDecimal(getDecimal("Valor_Rateio_" + quantidadeDeAcertos +"_Números")))
                .build();
    }

    public String getString(String columnName){
        return converterUtil.getColumnValue(columns, columnName);
    }

    public String getDecimal(String columnName){
        return converterUtil.getColumnValue(columns, columnName).replace(".","").replace(",", ".");
    }

    public void setLoteria(Loteria loteria) {
        this.loteria = loteria;
    }
}
