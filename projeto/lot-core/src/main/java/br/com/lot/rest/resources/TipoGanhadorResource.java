package br.com.lot.rest.resources;

import br.com.lot.rest.base.Item;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonRootName("tipo-ganhador")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoGanhadorResource extends Item<Long> {
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("loteria")
    private LoteriaResource loteria;
    @JsonProperty("quantidade-de-acertos")
    private Integer quantidadeDeAcertos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LoteriaResource getLoteria() {
        return loteria;
    }

    public void setLoteria(LoteriaResource loteria) {
        this.loteria = loteria;
    }

    public Integer getQuantidadeDeAcertos() {
        return quantidadeDeAcertos;
    }

    public void setQuantidadeDeAcertos(Integer quantidadeDeAcertos) {
        this.quantidadeDeAcertos = quantidadeDeAcertos;
    }
}
