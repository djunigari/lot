package br.com.lot.rest.exceptions;

public class InternalException extends RuntimeException {
    private static final long serialVersionUID = 7879654126020936614L;

    public static Integer CODE = 500666;

    public InternalException(String message, Exception e) {
        super(message, e);
    }

}