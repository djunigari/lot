package br.com.lot.core.model;

import br.com.lot.core.model.pks.DezenaPK;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
		@NamedQuery(name = Jogo.FIND_BY_LOTERIA_ID_AND_HASH,
				query = "From Jogo j where j.loteria.id = :loteriaId and  j.hash = :hash"),
})
@Table(name = "JOGO")
public class Jogo implements Serializable {
	public static final String FIND_BY_LOTERIA_ID_AND_HASH = "Jogo.findByLoteriaIdAndHash";
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	@Column(name = "JOGO_ID")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "LOTERIA_ID")
	private Loteria loteria;
	@Column(name = "HASH")
	private String hash;
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(
			name="JOGO_DEZENA",
			joinColumns=@JoinColumn(name="JOGO_ID", referencedColumnName="JOGO_ID"),
			inverseJoinColumns={@JoinColumn(name="DEZENAS_NUMERO", referencedColumnName="NUMERO"),
			@JoinColumn(name="DEZENAS_ORDEM", referencedColumnName="ORDEM"),
			@JoinColumn(name="DEZENAS_LOTERIA_ID", referencedColumnName="LOTERIA_ID")
			})
	private List<Dezena> dezenas;

	public Jogo(){}

	private Jogo(Builder builder) {
		this.id = builder.id;
		this.loteria = builder.loteria;
		this.dezenas = builder.dezenas;
		this.hash = builder.hash;
	}

	public static Builder newJogo() {
		return new Builder();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Loteria getLoteria() {
		return loteria;
	}

	public void setLoteria(Loteria loteria) {
		this.loteria = loteria;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public List<Dezena> getDezenas() {
		return dezenas;
	}

	public void setDezenas(List<Dezena> dezenas) {
		this.dezenas = dezenas;
	}

	public boolean hasDezenas(List<Dezena> dezenas){
		for(Dezena d : dezenas){
			if(!hasDezena(d)){
				return false;
			}
		}
		return true;
	}

	public boolean hasDezena(Dezena dezena){
		for(Dezena d : dezenas){
			if(d.equals(dezena)){
				return true;
			}
		}
		return false;
//		return dezenas.stream().filter(d -> d.equals(dezena)).findFirst().isPresent();
	}

	public static final class Builder {
		private Long id;
		private Loteria loteria;
		private List<Dezena> dezenas;
		private String hash;

		private Builder() {
			dezenas = new ArrayList<>();
		}

		public Jogo build() {
			StringBuilder sb = new StringBuilder("");
			for(Dezena d : dezenas){
				sb.append("[n");
				sb.append(d.getDezenaPK().getNumero());
				sb.append(",o");
				sb.append(d.getDezenaPK().getOrdem());
				sb.append("");
			}
			hash =  sb.toString();
			return new Jogo(this);
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder loteria(Loteria loteria) {
			this.loteria = loteria;
			return this;
		}

		public Builder addDezena(Dezena dezena){
			for(Dezena d : dezenas){
				if(d.equals(dezena)) {
					throw new IllegalArgumentException();
				}
			}
			this.dezenas.add(dezena);
			return this;
		}

		public Builder dezenas(List<Dezena> dezenas) {
			this.dezenas = dezenas;
			return this;
		}

		public Builder dezenas(int... numeros) {
			this.dezenas = new ArrayList<>();
			for(int i = 1; i <= 6; i++){
				dezenas.add(Dezena.newDezena()
						.dezenaPK(new DezenaPK(numeros[i-1], i, loteria.getId()))
						.build());
			}
			return this;
		}
	}
}
