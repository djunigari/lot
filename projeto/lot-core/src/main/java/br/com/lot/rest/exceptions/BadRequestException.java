package br.com.lot.rest.exceptions;

import br.com.lot.rest.base.Message;

import java.util.List;

public class BadRequestException extends RestfulException {
    private static final long serialVersionUID = -3312244518023511103L;
    private static final int BAD_REQUEST = 400;
    private List<Message> messages;

    public BadRequestException(String message, Integer code) {
        super(message, code, 400);
    }

    public BadRequestException(Integer code) {
        super("Invalid request", code, 400);
    }

    public BadRequestException(String message) {
        super(message, null, 400);
    }

    public BadRequestException() {
        super("Invalid request", null, 400);
    }

    public BadRequestException(String message, Integer code, List<Message> messages) {
        super(message, code, BAD_REQUEST);
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }
}
