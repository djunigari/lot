package br.com.lot.core.repositories;

import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.JogoAnalise;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Transactional
public class JogoAnaliseRepository extends  JPARepository<JogoAnalise,Long>{
    @Inject
    private EntityManager em;

    @Override
    public JogoAnalise create(JogoAnalise jogoAnalise) {
        JogoAnalise find = findByJogo(jogoAnalise.getJogo());
        if(find != null){
            return find;
        }
        return super.create(jogoAnalise);
    }


    public JogoAnalise findByJogo(Jogo jogo){
        List<JogoAnalise> jogoAnaliseList = em.createNamedQuery(JogoAnalise.FIND_BY_JOGO, JogoAnalise.class).setParameter("jogoId", jogo.getId()).setMaxResults(1).getResultList();
        if(jogoAnaliseList.size() == 0){
            return null;
        }
        return jogoAnaliseList.get(0);
    }

    @Override
    protected JogoAnalise find(JogoAnalise jogoAnalise) {
        if(jogoAnalise.getId() == null){
            return null;
        }
        return findById(jogoAnalise.getId());
    }

    @Override
    protected EntityManager entityManager() {
        return em;
    }
}
