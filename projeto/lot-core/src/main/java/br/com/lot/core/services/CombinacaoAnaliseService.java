package br.com.lot.core.services;

import br.com.lot.core.model.Combinacao;
import br.com.lot.core.model.CombinacaoAnalise;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.repositories.CombinacaoAnaliseRepository;
import br.com.lot.core.repositories.CombinacaoRepository;
import br.com.lot.core.repositories.SorteioRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class CombinacaoAnaliseService {
    @Inject
    private CombinacaoAnaliseRepository repository;
    @Inject
    private CombinacaoRepository combinacaoRepository;
    @Inject
    private SorteioRepository sorteioRepository;
    @Inject
    private IntervaloService intervaloService;

    public List<CombinacaoAnalise> create(){
        List<CombinacaoAnalise> result = new ArrayList<CombinacaoAnalise>();
        List<Combinacao> all = combinacaoRepository.findAll();
        for(Combinacao c : all){
            CombinacaoAnalise ca = repository.findByCombinacao(c);
            if(ca == null){
                ca = new CombinacaoAnalise(c);
                ca.setIntervalos(intervaloService.getIntervalos(c));
                repository.create(ca);
                result.add(ca);
            }else {
                Sorteio lastSorteio = sorteioRepository.findLastSorteioByCombinacaoId(c.getCombinacaoPK());
                if (lastSorteio != null) {
                    ca.setIntervalos(intervaloService.getIntervalos(c));
                    repository.update(ca);
                    result.add(ca);
                }
            }
        }
        return result;
    }
}
