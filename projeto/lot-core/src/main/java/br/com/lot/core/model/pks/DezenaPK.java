package br.com.lot.core.model.pks;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DezenaPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="NUMERO")
	private Integer numero;
	@Column(name="ORDEM")
	private Integer ordem;
	@Column(name="LOTERIA_ID")
	private Integer loteriaId;
	
	public DezenaPK(Integer numero,Integer ordem, Integer loteriaId){
		this.numero = numero;
		this.ordem = ordem;
		this.loteriaId = loteriaId;
	}
	
	public DezenaPK() {
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getLoteriaId() {
		return loteriaId;
	}

	public void setLoteriaId(Integer loteriaId) {
		this.loteriaId = loteriaId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DezenaPK) {
			DezenaPK pk = (DezenaPK) obj;
			if (!pk.getNumero().equals(numero)) {
				return false;
			}
			if(!pk.getOrdem().equals(ordem)){
				return false;
			}
			if(!pk.getLoteriaId().equals(loteriaId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return numero.hashCode() + loteriaId.hashCode();
	}

}
