package br.com.lot.rest.resources;

import br.com.lot.rest.base.Item;
import br.com.lot.rest.util.BasicDateSerializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JsonRootName("sorteio")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, creatorVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SorteioResource extends Item<Long> {
    @JsonProperty("concurso")
    private Integer concurso;
    @JsonProperty("loteria")
    private LoteriaResource loteria;
    @JsonProperty("data-sorteio")
    @JsonSerialize(using = BasicDateSerializer.class)
    private Date dataSorteio;
    @JsonProperty("jogo")
    private JogoResource jogo;
    @JsonProperty("arrecadacao-total")
    private BigDecimal arrecadacaoTotal;
    @JsonProperty("cidade")
    private String cidade;
    @JsonProperty("uf")
    private String uf;
    @JsonProperty("ganhadores")
    private List<GanhadorResource> ganhadores;
    @JsonProperty("valor-acumulado")
    private BigDecimal valorAcumulado;
    @JsonProperty("estimativa-premio")
    private BigDecimal estimativaPremio;
    @JsonProperty("valor-acumulado-especial")
    private BigDecimal valorAcumuladoEspecial;

    public Integer getConcurso() {
        return concurso;
    }

    public void setConcurso(Integer concurso) {
        this.concurso = concurso;
    }

    public LoteriaResource getLoteria() {
        return loteria;
    }

    public void setLoteria(LoteriaResource loteria) {
        this.loteria = loteria;
    }

    public Date getDataSorteio() {
        return dataSorteio;
    }

    public void setDataSorteio(Date dataSorteio) {
        this.dataSorteio = dataSorteio;
    }

    public JogoResource getJogo() {
        return jogo;
    }

    public void setJogo(JogoResource jogo) {
        this.jogo = jogo;
    }

    public BigDecimal getArrecadacaoTotal() {
        return arrecadacaoTotal;
    }

    public void setArrecadacaoTotal(BigDecimal arrecadacaoTotal) {
        this.arrecadacaoTotal = arrecadacaoTotal;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public List<GanhadorResource> getGanhadores() {
        return ganhadores;
    }

    public void setGanhadores(List<GanhadorResource> ganhadores) {
        this.ganhadores = ganhadores;
    }

    public BigDecimal getValorAcumulado() {
        return valorAcumulado;
    }

    public void setValorAcumulado(BigDecimal valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public BigDecimal getEstimativaPremio() {
        return estimativaPremio;
    }

    public void setEstimativaPremio(BigDecimal estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public BigDecimal getValorAcumuladoEspecial() {
        return valorAcumuladoEspecial;
    }

    public void setValorAcumuladoEspecial(BigDecimal valorAcumuladoEspecial) {
        this.valorAcumuladoEspecial = valorAcumuladoEspecial;
    }
}
