package br.com.lot.rest.util;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class ResponseUtils {

    private ResponseUtils() {
    }

    public static Response notAllowed(String... allowedMethods) {
        final Response.ResponseBuilder builder = Response
                .status(405);

        StringBuilder stringBuilder = new StringBuilder();

        for (String method : allowedMethods) {
            stringBuilder.append(method).append(", ");
        }

        stringBuilder.replace(stringBuilder.lastIndexOf(", "), stringBuilder.length(), "");
        builder.header("Allow", stringBuilder.toString());
        return builder.build();
    }

    public static Response created(Object o) {
        return Response
                .status(Response.Status.CREATED)
                .entity(o)
                .build();
    }

    public static Response accepted(Object o) {
        return Response
                .status(Response.Status.ACCEPTED)
                .entity(o)
                .build();
    }

    public static Response ok(Object o) {
        return Response
                .ok(o)
                .build();
    }

    public static Response notFound() {
        return Response
                .status(Response.Status.NOT_FOUND)
                .build();
    }

    public static Response created(String uri, Object o) {
        try {
            URI uriObject = new URI(uri);

            return Response
                    .created(uriObject)
                    .entity(o)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return created(o);
    }

    public static Response conflict() {
        return Response
                .status(Response.Status.CONFLICT)
                .build();
    }

    public static Response badRequest() {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .build();
    }

    public static Response conflict(Object o) {
        return Response
                .status(Response.Status.CONFLICT)
                .entity(o)
                .build();
    }

    public static Response response(Response.Status status, Object entity) {
        return Response
                .status(status)
                .entity(entity)
                .build();
    }

    public static Response response(Response.Status status, Map<String, Object> headers, Object entity) {
        Response.ResponseBuilder builder = Response
                .status(status)
                .entity(entity);

        for (Map.Entry<String, Object> stringObjectEntry : headers.entrySet()) {
            builder.header(stringObjectEntry.getKey(), stringObjectEntry.getValue());
        }

        return builder.build();
    }

    public static Response response(Response.Status status, Map<String, Object> headers) {
        return response(status, headers, null);
    }

    public static Response movedPermanently(String location) {
        return Response
                .status(Response.Status.MOVED_PERMANENTLY)
                .header("Location", location)
                .build();
    }

    public static Response seeOther(String location) {
        try {
            URI uriLocation = new URI(location);

            return Response
                    .seeOther(uriLocation)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }
}
