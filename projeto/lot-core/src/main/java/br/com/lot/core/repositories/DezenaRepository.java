package br.com.lot.core.repositories;

import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.pks.DezenaPK;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class DezenaRepository extends JPARepository<Dezena,DezenaPK>{
    @Inject
    private EntityManager em;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    public List<Dezena> create(List<Dezena> dezenas) {
        List<Dezena> list = new ArrayList<>();
        for(Dezena d : dezenas){
            list.add(create(d));
        }
        return list;
    }

    @Override
    public Dezena find(Dezena dezena) {
        if(dezena.getDezenaPK() == null){
            return  null;
        }
        return findById(dezena.getDezenaPK());
    }
}
