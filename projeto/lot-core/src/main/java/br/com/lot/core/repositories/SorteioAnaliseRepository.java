package br.com.lot.core.repositories;

import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.SorteioAnalise;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class SorteioAnaliseRepository extends JPARepository<SorteioAnalise,Integer>{
    @Inject
    private EntityManager em;

    @Inject
    private JogoAnaliseRepository jogoAnaliseRepository;

    @Override
    public EntityManager entityManager() {
        return this.em;
    }

    @Override
    public SorteioAnalise create(SorteioAnalise sorteioAnalise) {
        if(sorteioAnalise.getJogoAnalise().getId() == null){
            sorteioAnalise.setJogoAnalise(jogoAnaliseRepository.create(sorteioAnalise.getJogoAnalise()));
        }
        return super.create(sorteioAnalise);
    }

    @Override
    protected SorteioAnalise find(SorteioAnalise sorteioAnalise) {
        return findBySorteio(sorteioAnalise.getSorteio());
    }

    public SorteioAnalise findBySorteio(Sorteio sorteio){
        List<SorteioAnalise> sorteioAnalises = em.createNamedQuery(SorteioAnalise.FIND_BY_SORTEIO, SorteioAnalise.class)
                .setParameter("sorteio", sorteio)
                .getResultList();
        if(sorteioAnalises.size() == 0){
            return null;
        }
        return sorteioAnalises.get(0);
    }

    public List<SorteioAnalise> create(List<SorteioAnalise> sorteios){
        List<SorteioAnalise> result = new ArrayList<>();
        for(SorteioAnalise s : sorteios) {
            result.add(create(s));
        }
        return result;
    }
}
