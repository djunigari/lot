package br.com.lot.core.repositories;

import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.Jogo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class JogoRepository extends JPARepository<Jogo,Long>{
    @Inject
    private EntityManager em;

    @Inject
    private DezenaRepository dezenaRepository;

    @Override
    protected EntityManager entityManager() {
        return em;
    }

    @Override
    public Jogo create(Jogo jogo) {
        if(!validate(jogo)){
            throw new ValidationException();
        }

        List<Jogo> jogos = findByLoteriaIdAndHash(jogo.getLoteria().getId(),jogo.getHash());
        if(jogos.size() > 0){
            return jogos.get(0);
        }
        jogo.setDezenas(dezenaRepository.create(jogo.getDezenas()));
        return super.create(jogo);
    }

    @Override
    protected Jogo find(Jogo jogo) {
        if(jogo.getId() == null){
            return null;
        }
        return findById(jogo.getId());
    }

    public List<Jogo> findByLoteriaIdAndHash(Integer loteriaId,String hash){
        return em.createNamedQuery(Jogo.FIND_BY_LOTERIA_ID_AND_HASH,Jogo.class)
                .setParameter("loteriaId",loteriaId)
                .setParameter("hash",hash)
                .getResultList();
    }

    public List<Jogo> findByDezenas(List<Dezena> dezenas){
        List<Jogo> all = findAll();

        List<Jogo> result = new ArrayList<Jogo>();
        for(Jogo j : all){
            if(j.hasDezenas(dezenas)){
                result.add(j);
            }
        }
        return result;
    }

    public boolean validate(Jogo jogo){
        if(jogo.getDezenas().size() != jogo.getLoteria().getQuantidadeDeDezenas()){
            return false;
        }

        List<Dezena> dezenas = jogo.getDezenas();
        for(Dezena dezena : dezenas){
            int cont = 0;
            for(Dezena d : dezenas) {
                if (d.equals(dezena)) {
                    cont++;
                }
            }
            if(cont > 1){
                return false;
            }
        }

        for(Dezena dezena : dezenas){
            int cont = 0;
            for(Dezena d : dezenas) {
                if (d.getDezenaPK().getNumero().equals(dezena.getDezenaPK().getNumero())) {
                    cont++;
                }
            }
            if(cont > 1){
                return false;
            }
        }

        for(Dezena dezena : dezenas){
            int cont = 0;
            for(Dezena d : dezenas) {
                if (d.getDezenaPK().getOrdem().equals(dezena.getDezenaPK().getOrdem())) {
                    cont++;
                }
            }
            if(cont > 1){
                return false;
            }
        }
        return true;
    }

}
