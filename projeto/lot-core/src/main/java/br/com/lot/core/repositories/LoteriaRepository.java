package br.com.lot.core.repositories;

import br.com.lot.core.model.Loteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;

@Repository
@Transactional
public class LoteriaRepository extends JPARepository<Loteria,Integer>{
	@Inject
	private EntityManager em;

	@Override
	public EntityManager entityManager() {
		return this.em;
	}

	public Loteria findByNome(String nome){
		return em.createNamedQuery(Loteria.FIND_BY_NAME,Loteria.class).setParameter("nome",nome).getSingleResult();
	}

	@Override
	protected Loteria find(Loteria loteria) {
		if(loteria.getId() == null){
			return null;
		}
		return findById(loteria.getId());
	}
}
