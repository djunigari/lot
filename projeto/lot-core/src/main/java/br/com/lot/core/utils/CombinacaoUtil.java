package br.com.lot.core.utils;

import br.com.lot.core.model.Combinacao;
import br.com.lot.core.model.Dezena;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.repositories.CombinacaoRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CombinacaoUtil {
    private static HashMap<String, CombinacaoPool> pools = new HashMap<>();
    private Loteria loteria;
    private CombinacaoRepository repository;

    public CombinacaoUtil(Loteria loteria,CombinacaoRepository repository){
        this.loteria = loteria;
        this.repository = repository;
        if(pools.get(loteria.getNome()) == null){
            this.pools.put(loteria.getNome(),new CombinacaoPool(loteria,repository));
        }
    }

    public static CombinacaoPool getCombinacaoPool(String namePool){
        return pools.get(namePool);
    }

    public List<Combinacao> getCombinacoes(Jogo jogo){
        Integer max = jogo.getLoteria().getQuantidadeDeDezenas();
        List<Dezena> dezenas = jogo.getDezenas();
        ArrayList<Integer> numeros = new ArrayList<>();
        for(Dezena d : dezenas){
            numeros.add(d.getDezenaPK().getNumero());
        }
        List<Combinacao> combs = createCombinacao(numeros, 0, loteria.getQuantidadeDeDezenas()-1);
        Collections.sort(combs);
        return combs;
    }

   private List<Combinacao> createCombinacao(List<Integer> numeros, int first, int last){
       int aux = last - first;
       if(aux > 0){
           List<Combinacao> list1 = null;
           List<Combinacao> list2 = null;
           if(aux % 2 == 0){
               int x =  aux/2; // 1
               list1 = createCombinacao( numeros, first, first + x);
               list2 = createCombinacao( numeros, first + x + 1, last);
           }else{
               if(aux-1 > 0){
                   int x = (aux-1)/2;
                   list1 = createCombinacao( numeros, first, first + x);
                   list2 = createCombinacao( numeros, first + x + 1, last);

               }else{
                   list1 = createCombinacao( numeros, first, first);
                   list2 = createCombinacao( numeros, last, last);
               }
           }
           ArrayList<Combinacao> result = new ArrayList<>();
           result.addAll(list1);
           result.addAll(list2);
           for(Combinacao c1 : list1){
               for(Combinacao c2 :list2){
                   ArrayList<Integer> list = new ArrayList<>(c1.getNumeros());
                   list.addAll(c2.getNumeros());
                   Combinacao combinacao = Combinacao.newCombinacao()
                           .qtdeNumeros(c1.getQtdeNumeros() + c2.getQtdeNumeros())
                           .numeros(list)
                           .loteria(loteria)
                           .build();

                   Collections.sort(combinacao.getNumeros());
                   result.add(pools.get(loteria.getNome()).insert(combinacao));
               }
           }
           return result;
       }
       ArrayList<Combinacao> result = new ArrayList<>();

       Combinacao combinacao = Combinacao.newCombinacao()
               .qtdeNumeros(1)
               .addNumeros(numeros.get(first))
               .loteria(loteria)
               .build();
       result.add(pools.get(loteria.getNome()).insert(combinacao));
       return result;
   }

    public static void resetPools(){
        pools = new HashMap<>();
    }

//    public static void main(String[] args) {
//
//        Calendar inicio = Calendar.getInstance();
//
//        CombinacaoUtil util = new CombinacaoUtil();
//        ArrayList<Integer> numeros = new ArrayList<>();
//        numeros.add(1);
//        numeros.add(2);
//        numeros.add(3);
//        numeros.add(4);
//        numeros.add(5);
//        numeros.add(6);
//        numeros.add(7);
//        numeros.add(8);
//        numeros.add(9);
//        numeros.add(10);
//        numeros.add(11);
//        numeros.add(12);
//        numeros.add(13);
//        numeros.add(14);
//        numeros.add(15);
//
//        List<Combinacao> combs = util.createCombinacao(new Loteria(), numeros, 0, numeros.size()-1);
//        Collections.sort(combs);
//
//        for(Combinacao c : combs){
//            String prefix = "";
//            StringBuilder sb = new StringBuilder("[");
//            for(Integer i : c.getNumeros()){
//                sb.append(prefix);
//                prefix = ",";
//                sb.append(i);
//
//            }
//            sb.append("]");
//            System.out.println(sb.toString());
//        }
//
//        Calendar fim = Calendar.getInstance();
//        System.out.println(combs.size());
//        System.out.println(fim.getTimeInMillis()-inicio.getTimeInMillis());
//    }
}
