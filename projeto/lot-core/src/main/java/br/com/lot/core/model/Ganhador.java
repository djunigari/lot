package br.com.lot.core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class Ganhador implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private Integer id;
	@ManyToOne
	private TipoGanhador tipoDeGanhador;
	private Integer quantidadeDeGanhadores;
	private BigDecimal premioParaCada;

	public Ganhador(){}
	
	private Ganhador(Builder builder) {
		this.id = builder.id;
		this.tipoDeGanhador = builder.tipoDeGanhador;
		this.quantidadeDeGanhadores = builder.quantidadeDeGanhadores;
		this.premioParaCada = builder.premioParaCada;
	}

	public static Builder newGanhador() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	
	public TipoGanhador getTipoDeGanhador() {
		return tipoDeGanhador;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setTipoDeGanhador(TipoGanhador tipoDeGanhador) {
		this.tipoDeGanhador = tipoDeGanhador;
	}
	public Integer getQuantidadeDeGanhadores() {
		return quantidadeDeGanhadores;
	}
	public void setQuantidadeDeGanhadores(Integer quantidadeDeGanhadores) {
		this.quantidadeDeGanhadores = quantidadeDeGanhadores;
	}
	public BigDecimal getPremioParaCada() {
		return premioParaCada;
	}
	public void setPremioParaCada(BigDecimal premioParaCada) {
		this.premioParaCada = premioParaCada;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static final class Builder {
		private Integer id;
		private TipoGanhador tipoDeGanhador;
		private Integer quantidadeDeGanhadores;
		private BigDecimal premioParaCada;

		private Builder() {
		}

		public Ganhador build() {
			return new Ganhador(this);
		}

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder tipoDeGanhador(TipoGanhador tipoDeGanhador) {
			this.tipoDeGanhador = tipoDeGanhador;
			return this;
		}

		public Builder quantidadeDeGanhadores(Integer quantidadeDeGanhadores) {
			this.quantidadeDeGanhadores = quantidadeDeGanhadores;
			return this;
		}

		public Builder premioParaCada(BigDecimal premioParaCada) {
			this.premioParaCada = premioParaCada;
			return this;
		}
	}
}
