package br.com.lot.core.model.pks;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable 
public class SorteioPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CONCURSO")
	private Integer concurso;
	@Column(name="LOTERIA_ID")
	private Integer loteriaId;

	public SorteioPK(Integer concurso,Integer loteriaId ){
		this.concurso = concurso;
		this.loteriaId = loteriaId;
	}
	
	public SorteioPK() {
	}

	public Integer getLoteriaId() {
		return loteriaId;
	}

	public void setLoteriaId(Integer loteriaId) {
		this.loteriaId = loteriaId;
	}

	public Integer getConcurso() {
		return concurso;
	}

	public void setConcurso(Integer concurso) {
		this.concurso = concurso;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SorteioPK) {
			SorteioPK pk = (SorteioPK) obj;
			if (pk.getConcurso().equals(concurso)) {
				return false;
			}
			if (pk.getLoteriaId().equals(loteriaId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return concurso.hashCode() + loteriaId.hashCode();
	}

}
