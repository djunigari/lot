package br.com.lot.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.lot.core.model.pks.SorteioPK;

@Entity
public class Sorteio implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SorteioPK sorteioPK;
	@Temporal(value=TemporalType.DATE)
	private Date dataSorteio;
	@ManyToOne
	private Jogo jogo;
	private BigDecimal arrecadacaoTotal;
	private String cidade;
	private String uf;
	@ManyToMany
	private List<Ganhadores> ganhadores;
	private BigDecimal valorAcumulado;
	private BigDecimal estimativaPremio;
	private BigDecimal valorAcumuladoEspecial;

	public Sorteio(){}
	
	private Sorteio(Builder builder) {
		this.sorteioPK = builder.sorteioPK;
		this.dataSorteio = builder.dataSorteio;
		this.jogo = builder.jogo;
		this.arrecadacaoTotal = builder.arrecadacaoTotal;
		this.cidade = builder.cidade;
		this.uf = builder.uf;
		this.ganhadores = builder.ganhadores;
		this.valorAcumulado = builder.valorAcumulado;
		this.estimativaPremio = builder.estimativaPremio;
		this.valorAcumuladoEspecial = builder.valorAcumuladoEspecial;
	}

	public static Builder newSorteio() {
		return new Builder();
	}


	//GETTERS AND SETTERS
	public SorteioPK getSorteioPK() {
		return sorteioPK;
	}
	public void setSorteioPK(SorteioPK sorteioPK) {
		this.sorteioPK = sorteioPK;
	}
	public Date getDataSorteio() {
		return dataSorteio;
	}
	public void setDataSorteio(Date dataSorteio) {
		this.dataSorteio = dataSorteio;
	}
	public Jogo getJogo() {
		return jogo;
	}
	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}
	public BigDecimal getArrecadacaoTotal() {
		return arrecadacaoTotal;
	}
	public void setArrecadacaoTotal(BigDecimal arrecadacaoTotal) {
		this.arrecadacaoTotal = arrecadacaoTotal;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public List<Ganhadores> getGanhadores() {
		return ganhadores;
	}
	public void setGanhadores(List<Ganhadores> ganhadores) {
		this.ganhadores = ganhadores;
	}
	public BigDecimal getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(BigDecimal valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	public BigDecimal getEstimativaPremio() {
		return estimativaPremio;
	}
	public void setEstimativaPremio(BigDecimal estimativaPremio) {
		this.estimativaPremio = estimativaPremio;
	}
	public BigDecimal getValorAcumuladoEspecial() {
		return valorAcumuladoEspecial;
	}
	public void setValorAcumuladoEspecial(BigDecimal valorAcumuladoEspecial) {
		this.valorAcumuladoEspecial = valorAcumuladoEspecial;
	}


	public static final class Builder {
		private SorteioPK sorteioPK;
		private Date dataSorteio;
		private Jogo jogo;
		private BigDecimal arrecadacaoTotal;
		private String cidade;
		private String uf;
		private List<Ganhadores> ganhadores;
		private BigDecimal valorAcumulado;
		private BigDecimal estimativaPremio;
		private BigDecimal valorAcumuladoEspecial;

		private Builder() {
		}

		public Sorteio build() {
			return new Sorteio(this);
		}

		public Builder sorteioPK(SorteioPK sorteioPK) {
			this.sorteioPK = sorteioPK;
			return this;
		}

		public Builder dataSorteio(Date dataSorteio) {
			this.dataSorteio = dataSorteio;
			return this;
		}

		public Builder jogo(Jogo jogo) {
			this.jogo = jogo;
			return this;
		}

		public Builder arrecadacaoTotal(BigDecimal arrecadacaoTotal) {
			this.arrecadacaoTotal = arrecadacaoTotal;
			return this;
		}

		public Builder cidade(String cidade) {
			this.cidade = cidade;
			return this;
		}

		public Builder uf(String uf) {
			this.uf = uf;
			return this;
		}

		public Builder ganhadores(List<Ganhadores> ganhadores) {
			this.ganhadores = ganhadores;
			return this;
		}

		public Builder valorAcumulado(BigDecimal valorAcumulado) {
			this.valorAcumulado = valorAcumulado;
			return this;
		}

		public Builder estimativaPremio(BigDecimal estimativaPremio) {
			this.estimativaPremio = estimativaPremio;
			return this;
		}

		public Builder valorAcumuladoEspecial(BigDecimal valorAcumuladoEspecial) {
			this.valorAcumuladoEspecial = valorAcumuladoEspecial;
			return this;
		}
	}
}
