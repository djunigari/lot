package br.com.lot.core.converters.utils;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlConverterUtil {
	private Map<String,Integer> columns;
	
	public void setColumnsNames(Element row){
		Map<String,Integer> columns = new HashMap<>();
		Elements titles = row.select("th");
		for(int i =0; i < titles.size(); i++){
			columns.put(titles.get(i).text().toUpperCase(),i);
		}
		this.columns = columns;
	}
	
	public String getColumnValue(Elements columnsValues, String name){
		return columnsValues.get(columns.get(name.toUpperCase())).text();
	}
}
