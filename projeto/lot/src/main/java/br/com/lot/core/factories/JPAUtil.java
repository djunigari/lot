package br.com.lot.core.factories;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class JPAUtil {
	@Produces
	@PersistenceContext(unitName = "lot-hsqldb")
	private EntityManager em;
}
