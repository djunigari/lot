package br.com.lot.core.repositories;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.lot.core.model.Loteria;

@Stateless
public class LoteriaRepository extends JPARepository<Loteria,Integer>{
	@Inject
	private EntityManager em;

	@Override
	public EntityManager entityManager() {
		return this.em;
	}
}
