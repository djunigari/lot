package br.com.lot.core.model;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Loteria implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;
	private String nome;
	private Integer quantidadeDeDezenas;

	public Loteria(){}

	private Loteria(Builder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.quantidadeDeDezenas = builder.quantidadeDeDezenas;
	}

	public static Builder newLoteria() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQuantidadeDeDezenas() {
		return quantidadeDeDezenas;
	}
	public void setQuantidadeDeDezenas(Integer quantidadeDeDezenas) {
		this.quantidadeDeDezenas = quantidadeDeDezenas;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Loteria) {
			Loteria loteria = (Loteria) obj;
			if (loteria.getId().equals(id)) {
				return false;
			}
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return nome.hashCode()+quantidadeDeDezenas.hashCode();
	}


	public static final class Builder {
		private Integer id;
		private String nome;
		private Integer quantidadeDeDezenas;

		private Builder() {
		}

		public Loteria build() {
			return new Loteria(this);
		}

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder nome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder quantidadeDeDezenas(Integer quantidadeDeDezenas) {
			this.quantidadeDeDezenas = quantidadeDeDezenas;
			return this;
		}
	}
}
