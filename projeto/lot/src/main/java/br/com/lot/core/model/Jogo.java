package br.com.lot.core.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import br.com.lot.core.model.pks.JogoPK;

@Entity
public class Jogo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private JogoPK jogoPK;
	@ManyToMany(mappedBy="jogos")
	private List<Dezenas> dezenas;

	public Jogo(){}
	
	private Jogo(Builder builder) {
		this.jogoPK = builder.jogoPK;
		this.dezenas = builder.dezenas;
	}

	public static Builder newJogo() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	public JogoPK getJogoPK() {
		return jogoPK;
	}
	public void setJogoPK(JogoPK jogoPK) {
		this.jogoPK = jogoPK;
	}
	public List<Dezenas> getDezenas() {
		return dezenas;
	}
	public void setDezenas(List<Dezenas> dezenas) {
		this.dezenas = dezenas;
	}


	public static final class Builder {
		private JogoPK jogoPK;
		private List<Dezenas> dezenas;

		private Builder() {
		}

		public Jogo build() {
			return new Jogo(this);
		}

		public Builder jogoPK(JogoPK jogoPK) {
			this.jogoPK = jogoPK;
			return this;
		}

		public Builder dezenas(List<Dezenas> dezenas) {
			this.dezenas = dezenas;
			return this;
		}
	}
}
