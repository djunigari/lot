package br.com.lot.core.model;

import java.io.Serializable;
import java.lang.Integer;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import br.com.lot.core.model.pks.DezenasPK;

@Entity
public class Dezenas implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DezenasPK dezenasPK;
	private Integer ordem;
	@ManyToMany
	private List<Jogo> jogos;
	
	public Dezenas(){
		
	}

	private Dezenas(Builder builder) {
		this.dezenasPK = builder.dezenasPK;
		this.ordem = builder.ordem;
		this.jogos = builder.jogos;
	}

	public static Builder newDezenas() {
		return new Builder();
	}


	public DezenasPK getDezenasPK() {
		return dezenasPK;
	}
	public void setDezenasPK(DezenasPK dezenasPK) {
		this.dezenasPK = dezenasPK;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public List<Jogo> getJogos() {
		return jogos;
	}
	public void setJogos(List<Jogo> jogos) {
		this.jogos = jogos;
	}

	public static final class Builder {
		private DezenasPK dezenasPK;
		private Integer ordem;
		private List<Jogo> jogos;

		private Builder() {
		}

		public Dezenas build() {
			return new Dezenas(this);
		}

		public Builder dezenasPK(DezenasPK dezenasPK) {
			this.dezenasPK = dezenasPK;
			return this;
		}

		public Builder ordem(Integer ordem) {
			this.ordem = ordem;
			return this;
		}

		public Builder jogos(List<Jogo> jogos) {
			this.jogos = jogos;
			return this;
		}
	}
}
