package br.com.lot.core.converters;

import static br.com.lot.core.model.Dezenas.newDezenas;
import static br.com.lot.core.model.Ganhadores.newGanhadores;
import static br.com.lot.core.model.Jogo.newJogo;
import static br.com.lot.core.model.Sorteio.newSorteio;
import static br.com.lot.core.model.TipoGanhador.newTipoGanhador;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.lot.core.converters.utils.HtmlConverterUtil;
import br.com.lot.core.model.Dezenas;
import br.com.lot.core.model.Ganhadores;
import br.com.lot.core.model.Jogo;
import br.com.lot.core.model.Loteria;
import br.com.lot.core.model.Sorteio;
import br.com.lot.core.model.TipoGanhador;
import br.com.lot.core.model.pks.DezenasPK;
import br.com.lot.core.model.pks.JogoPK;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.core.model.pks.TipoGanhadorPK;
import br.com.lot.core.repositories.LoteriaRepository;

@Stateless
public class LotofacilRowTableToSorteioConverter extends Converter<Element,Sorteio>{
	private static final int QUANTIDADE_DE_COLUNAS = 33;
	
	private HtmlConverterUtil converterUtil;
	private Elements columns;
	@Inject 
	private LoteriaRepository loteriaRepository;
	@Inject 
	private HtmlToSorteioConverter con;
	private Loteria loteria;
	
	
	public LotofacilRowTableToSorteioConverter(HtmlConverterUtil converterUtil){
		this.converterUtil = converterUtil;
        this.loteria = loteriaRepository.find(1);
	}
	
    @Override
    public Sorteio convert(Element tr) {
        columns = tr.select("td");
        
        if(columns.size() != QUANTIDADE_DE_COLUNAS){
        	return null;
        }

        Integer concurso = new Integer(getString("Concurso"));
        Date dataSorteio;
		try {
			dataSorteio = new SimpleDateFormat("dd/MM/yyyy").parse(getString("Data Sorteio"));
		} catch (ParseException e) {
			e.printStackTrace();
			dataSorteio = null;
		}
        String cidade = getString("Cidade");
        String uf = getString("UF");
        BigDecimal arrecadacaoTotal = new BigDecimal(getDecimal("Arrecadacao_Total"));
        BigDecimal valorAcumulado = new BigDecimal(getDecimal("Acumulado_15_Números"));
        BigDecimal estimativaPremio = new BigDecimal(getDecimal("Estimativa_Premio"));
        BigDecimal valorAcumuladoEspecial = new BigDecimal(getDecimal("Valor_Acumulado_Especial"));
        SorteioPK sorteioPK = new SorteioPK(concurso,loteria.getId());
        Jogo jogo = createJogo();
        List<Ganhadores> ganhadores = new ArrayList<>();
        
        return newSorteio()
                .sorteioPK(sorteioPK)
                .dataSorteio(dataSorteio)
                .jogo(jogo)
                .arrecadacaoTotal(arrecadacaoTotal)
                .cidade(cidade)
                .uf(uf)
                .ganhadores(ganhadores)
                .valorAcumulado(valorAcumulado)
                .estimativaPremio(estimativaPremio)
                .valorAcumuladoEspecial(valorAcumuladoEspecial)
                .build();
    }
    
    public String getString(String columnName){
    	return converterUtil.getColumnValue(columns, columnName);
    }
    
    public String getDecimal(String columnName){
    	return converterUtil.getColumnValue(columns, columnName).replace(".","").replace(",", ".");
    }
    
    private Jogo createJogo(){
    	List<Dezenas> dezenas = new ArrayList<>();
    	
    	for(int i = 1; i <= loteria.getQuantidadeDeDezenas(); i++){
    		dezenas.add(getDezena(i));
    	}

    	return  newJogo()
    			.jogoPK(new JogoPK(new Integer(getString("Concurso")),loteria.getId()))
    			.dezenas(dezenas)
    			.build();
    }
    
    private Dezenas getDezena(Integer i){
    	return newDezenas()
    	    	.dezenasPK(new DezenasPK(new Integer(getString("Bola"+i)),loteria.getId()))
    	    	.ordem(i)
    	    	.build();
    }
    
    private Ganhadores getGanhadores(String nome, int quantidadeDeAcertos){
    	TipoGanhador tipoGanhador = newTipoGanhador()
    			.tipoGanhadorPK(new TipoGanhadorPK())
    			.nome(nome)
    			.quantidadeDeAcertos(quantidadeDeAcertos)
    			.build();
    	
    	return newGanhadores()
    			.tipoDeGanhador(tipoGanhador)
    			.quantidadeDeGanhadores(new Integer(getString("Concurso")))
    			.premioParaCada(new BigDecimal(getDecimal("Estimativa_Premio")))
    			.build();
    }
}
