package br.com.lot.core.model.pks;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class DezenasPK  implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer numero;
	private Integer loteriaId;
	
	public DezenasPK(Integer numero,Integer loteriaId ){
		this.numero = numero;
		this.loteriaId = loteriaId;
	}
	
	public DezenasPK() {
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getLoteriaId() {
		return loteriaId;
	}

	public void setLoteriaId(Integer loteriaId) {
		this.loteriaId = loteriaId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DezenasPK) {
			DezenasPK pk = (DezenasPK) obj;
			if (pk.getNumero().equals(numero)) {
				return false;
			}
			if (pk.getLoteriaId().equals(loteriaId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return numero.hashCode() + loteriaId.hashCode();
	}

}
