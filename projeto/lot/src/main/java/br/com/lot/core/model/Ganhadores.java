package br.com.lot.core.model;

import java.io.Serializable;
import java.lang.Integer;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Ganhadores implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private Integer id;
	private TipoGanhador tipoDeGanhador;
	private Integer quantidadeDeGanhadores;
	private BigDecimal premioParaCada;

	public Ganhadores(){}
	
	private Ganhadores(Builder builder) {
		this.id = builder.id;
		this.tipoDeGanhador = builder.tipoDeGanhador;
		this.quantidadeDeGanhadores = builder.quantidadeDeGanhadores;
		this.premioParaCada = builder.premioParaCada;
	}

	public static Builder newGanhadores() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	
	public TipoGanhador getTipoDeGanhador() {
		return tipoDeGanhador;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setTipoDeGanhador(TipoGanhador tipoDeGanhador) {
		this.tipoDeGanhador = tipoDeGanhador;
	}
	public Integer getQuantidadeDeGanhadores() {
		return quantidadeDeGanhadores;
	}
	public void setQuantidadeDeGanhadores(Integer quantidadeDeGanhadores) {
		this.quantidadeDeGanhadores = quantidadeDeGanhadores;
	}
	public BigDecimal getPremioParaCada() {
		return premioParaCada;
	}
	public void setPremioParaCada(BigDecimal premioParaCada) {
		this.premioParaCada = premioParaCada;
	}


	public static final class Builder {
		private Integer id;
		private TipoGanhador tipoDeGanhador;
		private Integer quantidadeDeGanhadores;
		private BigDecimal premioParaCada;

		private Builder() {
		}

		public Ganhadores build() {
			return new Ganhadores(this);
		}

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder tipoDeGanhador(TipoGanhador tipoDeGanhador) {
			this.tipoDeGanhador = tipoDeGanhador;
			return this;
		}

		public Builder quantidadeDeGanhadores(Integer quantidadeDeGanhadores) {
			this.quantidadeDeGanhadores = quantidadeDeGanhadores;
			return this;
		}

		public Builder premioParaCada(BigDecimal premioParaCada) {
			this.premioParaCada = premioParaCada;
			return this;
		}
	}
}
