package br.com.lot.core.model.pks;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TipoGanhadorPK implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer loteriaId;
	
	public TipoGanhadorPK(Integer id,Integer loteriaId ){
		this.id = id;
		this.loteriaId = loteriaId;
	}
	
	public TipoGanhadorPK() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoteriaId() {
		return loteriaId;
	}

	public void setLoteriaId(Integer loteriaId) {
		this.loteriaId = loteriaId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TipoGanhadorPK) {
			TipoGanhadorPK pk = (TipoGanhadorPK) obj;
			if (pk.getId().equals(id)) {
				return false;
			}
			if (pk.getLoteriaId().equals(loteriaId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return id.hashCode() + loteriaId.hashCode();
	}
}
