package br.com.lot.core.converters;


import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import br.com.lot.core.converters.utils.HtmlConverterUtil;
import br.com.lot.core.model.Sorteio;

@ApplicationScoped
public class HtmlToSorteioConverter extends Converter<String,List<Sorteio>>{
	@Inject
	private HtmlConverterUtil htmlConverterUtil;
	
	@Override
	public List<Sorteio> convert(String html) {
		Elements rows = Jsoup.parse(html)
				.select("table")
				.get(0)
				.select("tr");
		
		htmlConverterUtil.setColumnsNames(rows.get(0));

		LotofacilRowTableToSorteioConverter rowConverter = new LotofacilRowTableToSorteioConverter(htmlConverterUtil);

		List<Sorteio> sorteios = new ArrayList<Sorteio>();
		
		for(int i = 1 ; i < rows.size(); i++){
			Sorteio sorteio = rowConverter.convert(rows.get(i));
			if(sorteio == null){
				continue;
			}
			sorteios.add(sorteio);
		}
		return sorteios;
	}
}
