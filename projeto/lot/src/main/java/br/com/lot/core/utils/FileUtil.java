package br.com.lot.core.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class FileUtil {
    public  String fetchFile(String file) {
//    	Path path = Paths.get("src/main/resources/".concat(file));
//    	try {
//    		StringBuilder sb = new StringBuilder("");
//    		Stream<String> linhas = Files.lines(path,StandardCharsets.ISO_8859_1);
//			linhas.forEach(l -> sb.append(l));
//			linhas.close();
//			return sb.toString();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return null;
//		}
    	Path path = Paths.get("src/main/resources/".concat(file));
    	try {
			List<String> lines = Files.readAllLines(path,StandardCharsets.ISO_8859_1);
	    	StringBuilder sb = new StringBuilder("");
	    	for(String l : lines){
	    		sb.append(l);
	    	}
	        return sb.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    	
    	
    }
}
