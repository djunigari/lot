package br.com.lot.core.model;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import br.com.lot.core.model.pks.TipoGanhadorPK;

@Entity
public class TipoGanhador implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TipoGanhadorPK tipoGanhadorPK;
	private String nome;
	private Integer quantidadeDeAcertos;

	public TipoGanhador(){}
	
	private TipoGanhador(Builder builder) {
		this.tipoGanhadorPK = builder.tipoGanhadorPK;
		this.nome = builder.nome;
		this.quantidadeDeAcertos = builder.quantidadeDeAcertos;
	}

	public static Builder newTipoGanhador() {
		return new Builder();
	}

	//GETTERS AND SETTERS
	public TipoGanhadorPK getTipoGanhadorPK() {
		return tipoGanhadorPK;
	}
	public void setTipoGanhadorPK(TipoGanhadorPK tipoGanhadorPK) {
		this.tipoGanhadorPK = tipoGanhadorPK;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQuantidadeDeAcertos() {
		return quantidadeDeAcertos;
	}
	public void setQuantidadeDeAcertos(Integer quantidadeDeAcertos) {
		this.quantidadeDeAcertos = quantidadeDeAcertos;
	}


	public static final class Builder {
		private TipoGanhadorPK tipoGanhadorPK;
		private String nome;
		private Integer quantidadeDeAcertos;

		private Builder() {
		}

		public TipoGanhador build() {
			return new TipoGanhador(this);
		}

		public Builder tipoGanhadorPK(TipoGanhadorPK tipoGanhadorPK) {
			this.tipoGanhadorPK = tipoGanhadorPK;
			return this;
		}

		public Builder nome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder quantidadeDeAcertos(Integer quantidadeDeAcertos) {
			this.quantidadeDeAcertos = quantidadeDeAcertos;
			return this;
		}
	}
}
