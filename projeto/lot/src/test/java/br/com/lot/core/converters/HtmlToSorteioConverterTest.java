package br.com.lot.core.converters;

import br.com.lot.core.converters.utils.HtmlConverterUtil;
import br.com.lot.core.model.*;
import br.com.lot.core.model.pks.DezenasPK;
import br.com.lot.core.model.pks.JogoPK;
import br.com.lot.core.model.pks.SorteioPK;
import br.com.lot.core.model.pks.TipoGanhadorPK;
import br.com.lot.core.repositories.LoteriaRepository;
import br.com.lot.core.utils.DefaultArquillianTest;
import br.com.lot.core.utils.FileUtil;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static br.com.lot.core.model.Loteria.newLoteria;

import java.util.List;


@RunWith(Arquillian.class)
public class HtmlToSorteioConverterTest extends DefaultArquillianTest{
	@Inject
	private HtmlToSorteioConverter converter;
	@Inject 
	private LoteriaRepository loteriaRepository;

	@Deployment
	public static Archive<?> testArchive() {
		return createDefaultArchive()
				.addClasses(
						DezenasPK.class,
						Dezenas.class,
						Ganhadores.class,
						JogoPK.class,
						Jogo.class,
						Loteria.class,
						SorteioPK.class,
						Sorteio.class,
						TipoGanhadorPK.class,
						TipoGanhador.class,

						HtmlConverterUtil.class,
						Converter.class,
						HtmlToSorteioConverter.class,
						LotofacilRowTableToSorteioConverter.class,
						
						LoteriaRepository.class,
						Loteria.class,

						FileUtil.class
						);
	}

	
	@Test
	public void test() {
		loteriaRepository.create(newLoteria().nome("Lotofacil").quantidadeDeDezenas(15).build());
		String file = new FileUtil().fetchFile("D_LOTFAC.HTM");
		List<Sorteio> sorteios = converter.convert(file);
		for(Sorteio s : sorteios){
			System.out.println(s.getSorteioPK().getConcurso());
		}
	}
}
