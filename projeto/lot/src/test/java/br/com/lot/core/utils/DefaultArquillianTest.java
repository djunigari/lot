package br.com.lot.core.utils;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import br.com.lot.core.factories.JPAUtil;
import br.com.lot.core.repositories.JPARepository;

public class DefaultArquillianTest {
	
	public static WebArchive createDefaultArchive() { 
		return ShrinkWrap.create(WebArchive.class, "aplicacaoTeste.war") 
				.addClasses(JPARepository.class,
						JPAUtil.class)
				.addAsResource("test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml"); 
	}
}
